(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Program: VisuManager
 * File: VisuManager.st
 * Author: Bubenik
 * Created: Januar 14, 2016
 ********************************************************************
 * Implementation of program VisuManager
 ********************************************************************)

PROGRAM _INIT	
	gVisu.ChangePage := 0;													//Initial page with loading
	TARGETInfo_01(enable := 1,pOSVersion := ADR(OSVersionString));			//Check version of OS
	FOR i:=0 TO 2 DO
		Axis01_button[i] := 0;												//Reset all movement types on first axis
		Axis02_button[i] := 0;												//Reset all movement types on second axis
	END_FOR;
	Axis01_button[Axis01_Move_Mode] := 1;									//turn on only axis on position saved in battery 
	Axis02_button[Axis02_Move_Mode] := 1;									//turn on only axis on position saved in battery
	
	contentIndex := 0;
END_PROGRAM


PROGRAM _CYCLIC
	resultHandler;
	contentIndex;

	IF resultHandler THEN 		//result
		resultHandler := 0;		//reset result var
		contentIndex := contentIndex + 1;
	END_IF;
	
	loadingPercentage := (USINT_TO_REAL(contentIndex)/13)*100;
	
	IF gVisu.CurrentPage = 0 THEN											//condition is fulfilled only when turn on or restart
		InitTON(IN := TRUE,PT := T#10s);									//10 s loading
		IF InitTON.Q THEN
			gVisu.ChangePage := 1;											//Changes to Basic_control's first page	
			InitTON(IN := FALSE,PT := T#10s);							
		END_IF;
	END_IF;
	
	CASE gVisu.CurrentPage OF												//Just hold orange colour of main buttons
		1:	SDM_button_colour := FALSE;										//Basic Control orange button
			Basic_Control_button_colour := TRUE;
			Motion_button_colour := FALSE;
			Safety_button_colour := FALSE;
		11:	SDM_button_colour := FALSE;										//Motion orange button
			Basic_Control_button_colour := FALSE;
			Motion_button_colour := TRUE;
			Safety_button_colour := FALSE;
		20:	SDM_button_colour := FALSE;										//Safety orange button
			Basic_Control_button_colour := FALSE;
			Motion_button_colour := FALSE;
			Safety_button_colour := TRUE;
		30: SDM_button_colour := TRUE;										//SDM orange button
			Basic_Control_button_colour := FALSE;
			Motion_button_colour := FALSE;
			Safety_button_colour := FALSE;
	END_CASE;
	
	IF Axis01Disable = 0 OR Axis02Disable = 0 THEN							//One and more active Axis enables motion control
		MotionControl := 0;													
		
		FOR i:=0 TO 2 DO													//Check all changes in motion setup in loop for array[3] of buttons
			Axis01_R_TRIG_button[i](CLK:=Axis01_button[i]);					//check rise edge of axis01 buttons 
			IF Axis01_R_TRIG_button[i].Q THEN 								//When trigged
				Axis_move := AXIS01;										//Change switch on AXIS01 
			END_IF;
			Axis02_R_TRIG_button[i](CLK:=Axis02_button[i]);					//check rise edge of axis02 buttons
			IF Axis02_R_TRIG_button[i].Q THEN 								//When trigged
				Axis_move := AXIS02;										//Change switch on AXIS02
			END_IF;
			Axis01_Axis02_R_TRIG_button[i](CLK:=Axis01_Axis02_button[i]);	//check rise edge of axis01+axis02 buttons
			IF Axis01_Axis02_R_TRIG_button[i].Q THEN 						//When trigged
				Axis_move := AXIS01_AXIS02;									//Change switch on AXIS01_AXIS02
			END_IF;
		END_FOR;
	
		CASE Axis_move OF													//switch for Visu of motion buttons and Axis movement type	
			AXIS01: 
				FOR i:=0 TO 2 DO											 
					Axis01_button[i] := Axis01_R_TRIG_button[i].Q;			//In same cycle R_Trig.Q are true only on changed button and others are false 
					IF Axis01_R_TRIG_button[i].Q THEN						//Find changed button
						Axis01_Move_Mode:= i;								//and save to value for Axis01.st movement type	
					END_IF;
					IF Axis01_Axis02_button[i] = 1 THEN						//If before was selected Axis01+Axis02 button				
						Axis02_button[0] := 1;								//Automatically turn on first Axis02 button
					END_IF;
					Axis01_Axis02_button[i] := 0;							//And turn of all Axis01+Axis02 Buttons
				END_FOR;
			Axis_move := IDLE;												//After successful button change go to IDLE state for better performance
				
			AXIS02: 
				FOR i:=0 TO 2 DO	
					Axis02_button[i] := Axis02_R_TRIG_button[i].Q;			//In same cycle R_Trig.Q are true only on changed button and others are false
					IF Axis02_R_TRIG_button[i].Q THEN						//Find changed button
						Axis02_Move_Mode:= i;								//and save to value for Axis02.st movement type
					END_IF;
					IF Axis01_Axis02_button[i] = 1 THEN						//If before was selected Axis01+Axis02 button
						Axis01_button[0] := 1;								//Automatically turn on first Axis02 button
					END_IF;
					Axis01_Axis02_button[i] := 0;							//And turn of all Axis01+Axis02 Buttons
				END_FOR;
			Axis_move := IDLE;												//After successful button change go to IDLE state for better performance
			
			AXIS01_AXIS02:
				FOR i:=0 TO 2 DO
					Axis01_Axis02_button[i] :=  Axis01_Axis02_R_TRIG_button[i].Q;	//In same cycle R_Trig.Q are true only on changed button and others are false
					Axis01_button[i] := 0;									//Reset all Axis01 Buttons
					Axis02_button[i] := 0;									//Reset all Axis02 Buttons
				END_FOR;
				Axis01_button[2] := 1;
				Axis02_button[2] := 1;
			Axis_move := IDLE;												//After successful button change go to IDLE state for better performance
			IDLE:															//Do nothing
		END_CASE;
		
	ELSE 																	//if not active any axis 
		IF 9 < gVisu.CurrentPage AND gVisu.CurrentPage < 20 THEN			//and page on visu is on any motion page
			gVisu.ChangePage := 1;											//change to Basic controls first page
		END_IF;
		MotionControl := 1;													//turn of Main motion button
	END_IF;
	
	SafetyVisu := 1;														
	
END_PROGRAM


