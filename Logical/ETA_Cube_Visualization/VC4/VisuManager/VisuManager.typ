(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Program: VisuManager
 * File: VisuManager.typ
 * Author: Bubenik
 * Created: June 12, 2014
 ********************************************************************
 * Local data types of program VisuManager
 ********************************************************************)

TYPE
	AXIS_MOVE_TYPE : 
		( (*Show active button in Visu and change movement type in AxisXX.st*)
		AXIS01, (*Axis01 buttons*)
		AXIS02, (*Axis02 buttons*)
		AXIS01_AXIS02, (*Axis01+Axis02 buttons*)
		IDLE (*Do nothing*)
		);
END_TYPE
