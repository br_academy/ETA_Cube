(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * PROGRAM: Motion
 * File: Axis02.st
 * Author: Bubenik
 * Created: May 3, 2016
 ********************************************************************
 * Implementation OF PROGRAM Axis02
 ********************************************************************)

PROGRAM _INIT	 	
	AxisPar02.Velocity := 				1000; 			//set basic paraemters
	AxisPar02.Distance := 				1000; 			//set basic paraemters
	MpAxisBasic_02.MpLink := 			ADR(gAxisBasicLink_02); // ***do�asn� koment***
	MpAxisBasic_02.Axis := 				ADR(gAxis02);
	MpAxisBasic_02.Enable := 			TRUE;
	MpAxisBasic_02.Parameters := 		ADR(AxisPar02);
	brsstrcpy(ADR(action_text),ADR('Disconnected'));
END_PROGRAM

(*	motion_axis02_state enum
	DISCONNECTED			0
	INIT					1
	POWER_ON				2
	HOMING					3
	STANDSTILL				4
	MOVE1					5
	MOVE2					6
	MOVE3					7
	ERROR					8
*)

PROGRAM _CYCLIC

	MpAxisBasic_02();																						//mapp function call

	IF MpAxisBasic_02.Error = TRUE THEN																		//Error condition are checked all the time also when ACOPOS is disconnected
		IF MpAxisBasic_02.Info.Diag.Internal.Code = 1012 AND motion_axis02_state = DISCONNECTED THEN		//Check for error 1012 and disconnected case, if true it mean that this is not valid error
			motion_axis02_state := INIT;																	//Then Case are changed to Disconnected	
			MpAxisBasic_02.ErrorReset := FALSE;																//Just in case reset ErrorReset
		ELSE motion_axis02_state := ERROR;																	//Else it is valid error
		END_IF;
	END_IF;	
	
	IF MpAxisBasic_02.Info.BootState = mpAXIS_BLP_NETWORK_INACTIVE AND MpAxisBasic_02.Error = TRUE THEN		//When is ACOPOS disconnected, Mapp component reports an error, therefore it is not valid error 
		motion_axis02_state := DISCONNECTED;																//And Case is changed to Disconnected 
		MpAxisBasic_02.ErrorReset := FALSE;																	//Just in case reset ErrorReset
	END_IF;
	
	CASE motion_axis02_state OF																				//Main switch for setup, movement type and error case
		DISCONNECTED:																						//Switch are locked in this case when ACOPOS is disconnected
			IF MpAxisBasic_02.Info.BootState = mpAXIS_BLP_DONE OR MpAxisBasic_01.Info.BootState = mpAXIS_BLP_NETWORK_INIT_STARTED THEN	//check boot status of ACOPOS
				motion_axis02_state := INIT;																//If is connected can continue to init case 
				Axis02Disable := 0;																			//Allow motion button in Visu
				brsstrcpy(ADR(action_text),ADR('Connected'));												//Write action text into Axis info into Visu	
			ELSE 
				Axis02Disable := 1;																			//if not connected hide motion control in Visu
				brsstrcpy(ADR(action_text),ADR('Disconnected'));											//Write action text into Axis info into Visu
			END_IF;
		
		INIT:																								//Waiting if ACOPOS is ready to power on
			brsstrcpy(ADR(action_text),ADR('RdyToPowerOn'));												//Write action text into Axis info into Visu
			MpAxisBasic_02.Power := FALSE;																	//Reset power, in case of restart INIT CASE of ACOPOS
			IF (MpAxisBasic_02.Active = TRUE) AND (MpAxisBasic_02.Info.ReadyToPowerOn = TRUE) THEN			//ACOPOS must be active and ready for power on
				motion_axis02_state := POWER_ON;															//Change state to Power_on
			END_IF
		
		POWER_ON:																							//Status turn on power 
			brsstrcpy(ADR(action_text),ADR('Power On'));													//Write action text into Axis info into Visu
			MpAxisBasic_02.Power := TRUE;																	//Turn on power
			IF MpAxisBasic_02.PowerOn THEN																	//Wait while ACOPOS is powered on
				motion_axis02_state := HOMING;																//Change state to Homing
			END_IF;
		
		HOMING:																								//Sets home position to 0
			brsstrcpy(ADR(action_text),ADR('Homing'));														//Write action text into Axis info into Visu
			MpAxisBasic_02.Home := TRUE;																	//Turn on home reguest	
			IF MpAxisBasic_02.IsHomed THEN																	//Wait while ACOPOS is homed
				MpAxisBasic_02.Home := FALSE;																//After successful homing, variable Home can be reset for next using
				motion_axis02_state := STANDSTILL;															//Change state to standstill
			END_IF;
					
		STANDSTILL:																							//In this case is all successful initialized, when type of move is changed, it will come back to standstill 
			brsstrcpy(ADR(action_text),ADR('Standstill'));													//Write action text into Axis info into Visu
			MpAxisBasic_02.Stop := FALSE;																	//Is needed reset all possible actions
			MpAxisBasic_02.JogPositive:= FALSE;
			MpAxisBasic_02.JogNegative:= FALSE;
			MpAxisBasic_02.Home := FALSE;
			MpAxisBasic_02.MoveAbsolute := FALSE;
			MpAxisBasic_02.ErrorReset := FALSE;
			Axis02_Move1 := INIT_ACTION;																	//set up Init action for MOVE1 case
			Axis02_Move2 := INIT_DIR;																		//set up Init action for MOVE2 case
			IF  MpAxisBasic_02.Velocity = 0 AND MpAxisBasic_02.MoveActive = FALSE AND MpAxisBasic_02.CommandBusy = FALSE THEN	//Motor cant move, and command cant be busy
				motion_axis02_state := Axis02_Move_Mode+ CASE_OFFSET;										//choose Move type from VisuManager, offset is needed for order of states of case
			END_IF;	
		(*---------------------------------------------MOVE1----------------------------------------------*)			
		MOVE1:																								//Motor just accelerating and decelerating
			IF Axis02_Move_Mode+CASE_OFFSET <> MOVE1 THEN													//all the time of MOVE1 check if user dont change Move type
				MpAxisBasic_02.Stop := TRUE;																//if true stop the action
				motion_axis02_state := STANDSTILL;															//and change case to standstill
			END_IF;
			
			CASE Axis02_Move1 OF																			//Internal case for Move1 directions	
				INIT_ACTION:																				//Initial case are executed just once after move change
					brsstrcpy(ADR(action_text),ADR('Move1'));												//Write action text into Axis info into Visu
					IF  MpAxisBasic_02.Velocity = 0 THEN													//Motor cant move for init action
						AxisPar02.Jog.Acceleration := ACLRTN_MOVE1;											//Acceleration is set up for slow acceleration to full speed
						AxisPar02.Jog.Deceleration := DCLRTN_MOVE1;											//Deceleration is set up for slow decelation to almost zero speed
						AxisPar02.Jog.Velocity := VLCTY_MOVE1;												//Full speed value
						MpAxisBasic_02.JogNegative := TRUE;													//First motor will turn into negative direction
						Axis02_Move1 := ACTION1;															//Can continue in movement
					END_IF;
		
				ACTION1:
					IF  MpAxisBasic_02.Position <= 0 THEN													//If Negative direction reach position 0 changes direction to positive													
						MpAxisBasic_02.JogNegative := FALSE;												//Turn of Negative movement
						IF MpAxisBasic_02.Velocity < 100 THEN												//Wait while velocity is less than 100
							MpAxisBasic_02.JogPositive := TRUE;												//Change movement to positive
							Axis02_Move1 := ACTION2;														//Change case to action 2
						END_IF;
					END_IF;

				ACTION2:
					IF MpAxisBasic_02.Position >= 15000 THEN												//If Positive direction reach position 15000 changes direction to negative
						MpAxisBasic_02.JogPositive := FALSE;												//Turn of Positive movement
						IF MpAxisBasic_02.Velocity < 100 THEN												//Wait while velocity is less than 100 
							MpAxisBasic_02.JogNegative := TRUE;												//Change movement to positive
							Axis02_Move1 := ACTION1;														//Change case to action 1
						END_IF;
					END_IF;
			END_CASE;
		(*---------------------------------------------MOVE2----------------------------------------------*)			
		MOVE2:																								//Motor positon change by absolut movement into 4 directiories
			IF Axis02_Move_Mode+CASE_OFFSET <> MOVE2 THEN													//all the time of MOVE1 check if user dont change Move type
				MpAxisBasic_02.Stop := TRUE;																//if true stop the action
				motion_axis02_state := STANDSTILL;															//and change case to standstill
			END_IF;
			
			CASE Axis02_Move2 OF																			//Internal case for Move2 directions
				INIT_DIR:																					//Initial case are executed just once after move change
					brsstrcpy(ADR(action_text),ADR('Move2'));												//Write action text into Axis info into Visu
					MpAxisBasic_02.Home := TRUE;															//For absolut movement is needed to set up home position 0
					AxisPar02.Acceleration :=	ACLRTN_MOVE2;												//Acceleration is set up for very fast acceleration
					AxisPar02.Deceleration :=	DCLRTN_MOVE2;												//Deceleration is set up for very fast stop
					AxisPar02.Velocity :=		VLCTY_MOVE2;												//Full speed value
					IF MpAxisBasic_02.IsHomed AND MpAxisBasic_02.Position = 0 THEN 							//When Acopos is homed and position is 0 case can be changed to DIR1 
						MpAxisBasic_02.Home := FALSE;														//reset Home for next using
						Axis02_Move2 := DIR1;																//DIR1
					END_IF;
				
				DIR1:
					AxisPar02.Position := 1000;																//Position are dynamically changed 
					MpAxisBasic_02.MoveAbsolute := TRUE;													//Change to this position
					IF  MpAxisBasic_02.Position = 1000 THEN													//If position is reached stop movement and change state
						MpAxisBasic_02.MoveAbsolute := FALSE;												//stop movement
						Axis02_Move2 := DIR2;																//Direction 2
					END_IF;
				DIR2:
					AxisPar02.Position := 500;																//Position are dynamically changed 	
					MpAxisBasic_02.MoveAbsolute := TRUE;													//Change to this position
					IF MpAxisBasic_02.Position = 500 THEN													//If position is reached stop movement and change state
						MpAxisBasic_02.MoveAbsolute := FALSE;												//stop movement
						Axis02_Move2 := DIR3;																//Direction 3
					END_IF;
				
				DIR3:
					AxisPar02.Position := 1500;																//Position are dynamically changed 	
					MpAxisBasic_02.MoveAbsolute := TRUE;													//Change to this position
					IF MpAxisBasic_02.Position = 1500 THEN													//If position is reached stop movement and change state
						MpAxisBasic_02.MoveAbsolute := FALSE;												//stop movement
						Axis02_Move2 := DIR4;																//Direction 4
					END_IF;
					
				DIR4:
					AxisPar02.Position := 0;																//Position are dynamically changed	
					MpAxisBasic_02.MoveAbsolute := TRUE;													//Change to this position
					IF MpAxisBasic_02.Position = 0 THEN														//If position is reached stop movement and change state
						MpAxisBasic_02.MoveAbsolute := FALSE;												//stop movement
						Axis02_Move2 := DIR1;																//Direction 1
					END_IF;
			END_CASE;
		
		(*---------------------------------------------MOVE3----------------------------------------------*)			
		MOVE3:																								//In this movement motor do nothing, is used for example for autotuning
			brsstrcpy(ADR(action_text),ADR('Move3'));														//Write action text into Axis info into Visu
			motion_axis02_state := Axis02_Move_Mode+CASE_OFFSET;											//and change case to standstill

		ERROR:																								//Show most common error
			MpAxisBasic_02.JogPositive:= FALSE;																//Reset JogPositive
			MpAxisBasic_02.JogNegative:= FALSE;																//Reset JogNegative
			MpAxisBasic_02.Home := FALSE;																	//Reset Home
			MpAxisBasic_02.MoveAbsolute := FALSE;															//Reset MoveAbsolute
			Axis02_Error := MpAxisBasic_02.Info.Diag.Internal.Code;											//Save Error number into Case variable
			
			CASE Axis02_Error OF
				NO_ERROR:																					//If Error is 0(no error) change state do disconnected
					brsstrcpy(ADR(action_text),ADR('NO ERROR'));											//Write action text into Axis info into Visu
					motion_axis02_state := DISCONNECTED;
				
				LAG_ERROR:																					//lag error is needed to reset from Visu
					brsstrcpy(ADR(action_text),ADR('LAG ERROR!'));											//Write action text into Axis info into Visu
				
				HOMING_ERROR:																				//Homing error, needed to reset from Visu
					brsstrcpy(ADR(action_text),ADR('HOMING ERROR!'));										//Write action text into Axis info into Visu
				
				BAD_REQUEST:																				//Bad request, needed to reset from Visu
					brsstrcpy(ADR(action_text),ADR('BAD REQUEST!'));										//Write action text into Axis info into Visu
				
				UNPLUGGED:																					//When ACOPOS is unplugged, error is resolved automatically
					brsstrcpy(ADR(action_text),ADR('DISCONNECTED'));										//Write action text into Axis info into Visu
					MpAxisBasic_02.ErrorReset := TRUE;														//Error reset are true
					IF MpAxisBasic_02.Error = FALSE THEN													//When is Error solved
						MpAxisBasic_02.ErrorReset := FALSE;													//reset ErrorReset
						motion_axis02_state := DISCONNECTED;												//Change state do Disconnected
					END_IF;
				
				COMMUNICATION_LOST:																			//When ACOPOS lost comunication
					brsstrcpy(ADR(action_text),ADR('DISCONNECTED'));										//Write action text into Axis info into Visu
					MpAxisBasic_02.ErrorReset := FALSE;
					motion_axis02_state := DISCONNECTED;													//Change state do Disconnected
				
				BREAKDOWN:																					//After plug ACOPOS it will call Error					
					brsstrcpy(ADR(action_text),ADR('DISCONNECTED'));										//Write action text into Axis info into Visu
					MpAxisBasic_02.ErrorReset := TRUE;														//Error reset are true
					IF MpAxisBasic_02.Error = FALSE THEN													//When is Error solved
						MpAxisBasic_02.ErrorReset := FALSE;													//reset ErrorReset
					END_IF;
					motion_axis02_state := DISCONNECTED;													//Change state do Disconnected, but also when condition is not fulfiled fulfilled		
				ELSE																						//If is not any common error write only Error: error number
					brsitoa(MpAxisBasic_02.Info.Diag.Internal.Code,ADR(action_text_tmp));					//change number to string
					brsstrcpy(ADR(action_text),ADR('ERROR: '));
					brsstrcat(ADR(action_text),ADR(action_text_tmp));										//Connect Error and error number into one string
			END_CASE;
	END_CASE;
END_PROGRAM
