(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * PROGRAM: Motion
 * File: Axis01.st
 * Author: Bubenik
 * Created: April 21, 2016
 ********************************************************************
 * Implementation OF PROGRAM Axis01
 ********************************************************************)

PROGRAM _INIT	 	
	AxisPar01.Velocity := 				1000; 			//set basic paraemters
	AxisPar01.Distance := 				1000; 			//set basic paraemters
	MpAxisBasic_01.MpLink := 			ADR(gAxisBasicLink_01); // ***do�asn� koment***
	MpAxisBasic_01.Axis := 				ADR(gAxis01);
	MpAxisBasic_01.Enable := 			TRUE;
	MpAxisBasic_01.Parameters := 		ADR(AxisPar01);
	brsstrcpy(ADR(action_text),ADR('Disconnected'));
END_PROGRAM

(*	motion_axis01_state enum
	DISCONNECTED			0
	INIT					1
	POWER_ON				2
	HOMING					3
	STANDSTILL				4
	MOVE1					5
	MOVE2					6
	MOVE3					7
	ERROR					8
*)

PROGRAM _CYCLIC
	
	MpAxisBasic_01();																						//mapp function call
	
	IF MpAxisBasic_01.Error = TRUE THEN																		//Error condition are checked all the time also when ACOPOS is disconnected
		IF MpAxisBasic_01.Info.Diag.Internal.Code = 1012 AND motion_axis01_state = DISCONNECTED THEN		//Check for error 1012 and disconnected case, if true it mean that this is not valid error
			motion_axis01_state := DISCONNECTED;															//Then Case are changed to Disconnected
			MpAxisBasic_01.ErrorReset := FALSE;																//Just in case reset ErrorReset
		ELSE motion_axis01_state := ERROR;																	//Else it is valid error
		END_IF;
	END_IF;	
	
	IF MpAxisBasic_01.Info.BootState = mpAXIS_BLP_NETWORK_INACTIVE AND MpAxisBasic_01.Error = TRUE THEN		//When is ACOPOS disconnected, Mapp component reports an error, therefore it is not valid error 
		motion_axis01_state := DISCONNECTED;																//And Case is changed to Disconnected 
		MpAxisBasic_01.ErrorReset := FALSE;																	//Just in case reset ErrorReset
	END_IF;
	
	CASE motion_axis01_state OF																				//Main switch for setup, movement type and error case
		DISCONNECTED:																						//Switch are locked in this case when ACOPOS is disconnected												
			IF MpAxisBasic_01.Info.BootState = mpAXIS_BLP_DONE OR MpAxisBasic_01.Info.BootState = mpAXIS_BLP_NETWORK_INIT_STARTED THEN	//check boot status of ACOPOS
				motion_axis01_state := INIT;																//If is connected can continue to init case 
				Axis01Disable := 0; 																			//Allow motion button in Visu
				brsstrcpy(ADR(action_text),ADR('Connected'));												//Write action text into Axis info into Visu
			ELSE 
				Axis01Disable := 1;																				//if not connected hide motion control in Visu
				brsstrcpy(ADR(action_text),ADR('Disconnected'));											//Write action text into Axis info into Visu
			END_IF;
		
		INIT:																								//Waiting if ACOPOS is ready to power on
			brsstrcpy(ADR(action_text),ADR('RdyToPowerOn'));												//Write action text into Axis info into Visu
			MpAxisBasic_01.Power := FALSE;																	//Reset power, in case of restart INIT CASE of ACOPOS
			IF (MpAxisBasic_01.Active = TRUE) AND (MpAxisBasic_01.Info.ReadyToPowerOn = TRUE) THEN			//ACOPOS must be active and ready for power on
				motion_axis01_state := POWER_ON;															//Change state to Power_on
			END_IF
		
		POWER_ON:																							//Status turn on power
			brsstrcpy(ADR(action_text),ADR('Power On'));													//Write action text into Axis info into Visu
			MpAxisBasic_01.Power := TRUE;																	//Turn on power
			IF MpAxisBasic_01.PowerOn THEN																	//Wait while ACOPOS is powered on
				motion_axis01_state := HOMING;																//Change state to Homing
			END_IF;
		
		HOMING:																								//Sets home position to 0
			brsstrcpy(ADR(action_text),ADR('Homing'));														//Write action text into Axis info into Visu
			MpAxisBasic_01.Home := TRUE;																	//Turn on home reguest														
			IF MpAxisBasic_01.IsHomed THEN																	//Wait while ACOPOS is homed
				MpAxisBasic_01.Home := FALSE;																//After successful homing, variable Home can be reset for next using
				motion_axis01_state := STANDSTILL;															//Change state to standstill
			END_IF;
					
		STANDSTILL:																							//In this case is all successful initialized, when type of move is changed, it will come back to standstill 	
			brsstrcpy(ADR(action_text),ADR('Standstill'));													//Write action text into Axis info into Visu
			MpAxisBasic_01.Stop := FALSE;																	//Is needed reset all possible actions
			MpAxisBasic_01.JogPositive:= FALSE;
			MpAxisBasic_01.JogNegative:= FALSE;
			MpAxisBasic_01.Home := FALSE;
			MpAxisBasic_01.MoveAbsolute := FALSE;
			MpAxisBasic_01.ErrorReset := FALSE;
			Axis01_Move1 := INIT_ACTION;																	//set up Init action for MOVE1 case
			Axis01_Move2 := INIT_DIR;																		//set up Init action for MOVE2 case
			IF  MpAxisBasic_01.Velocity = 0 AND MpAxisBasic_01.MoveActive = FALSE AND MpAxisBasic_01.CommandBusy = FALSE THEN	//Motor cant move, and command cant be busy
				motion_axis01_state := Axis01_Move_Mode+CASE_OFFSET;										//choose Move type from VisuManager, offset is needed for order of states of case
			END_IF;
		(*---------------------------------------------MOVE1----------------------------------------------*)		
		MOVE1:																								//Motor just accelerating and decelerating
			IF Axis01_Move_Mode+CASE_OFFSET <> MOVE1 THEN													//all the time of MOVE1 check if user dont change Move type
				MpAxisBasic_01.Stop := TRUE;																//if true stop the action
				motion_axis01_state := STANDSTILL;															//and change case to standstill
			END_IF;
			
			CASE Axis01_Move1 OF																			//Internal case for Move1 directions
				INIT_ACTION:																				//Initial case are executed just once after move change
					brsstrcpy(ADR(action_text),ADR('Move1'));												//Write action text into Axis info into Visu						
					IF  MpAxisBasic_01.Velocity = 0 THEN													//Motor cant move for init action
						AxisPar01.Jog.Acceleration := ACLRTN_MOVE1;											//Acceleration is set up for slow acceleration to full speed
						AxisPar01.Jog.Deceleration := DCLRTN_MOVE1;											//Deceleration is set up for slow decelation to almost zero speed
						AxisPar01.Jog.Velocity := VLCTY_MOVE1;												//Full speed value
						MpAxisBasic_01.JogNegative := TRUE;													//First motor will turn into negative direction
						Axis01_Move1 := ACTION1;															//Can continue in movement
					END_IF;
		
				ACTION1:					
					IF  MpAxisBasic_01.Position <= 0 THEN													//If Negative direction reach position 0 changes direction to positive
						MpAxisBasic_01.JogNegative := FALSE;												//Turn of Negative movement
						IF MpAxisBasic_01.Velocity < 100 THEN												//Wait while velocity is less than 100 
							MpAxisBasic_01.JogPositive := TRUE;												//Change movement to positive
							Axis01_Move1 := ACTION2;														//Change case to action 2
						END_IF;
					END_IF;

				ACTION2:
					IF MpAxisBasic_01.Position >= 15000 THEN												//If Positive direction reach position 15000 changes direction to negative
						MpAxisBasic_01.JogPositive := FALSE;												//Turn of Positive movement
						IF MpAxisBasic_01.Velocity < 100 THEN												//Wait while velocity is less than 100 
							MpAxisBasic_01.JogNegative := TRUE;												//Change movement to positive
							Axis01_Move1 := ACTION1;														//Change case to action 1
						END_IF;
					END_IF;
			END_CASE;
		(*---------------------------------------------MOVE2----------------------------------------------*)			
		MOVE2:																								//Motor positon change by absolut movement into 4 directiories
			IF Axis01_Move_Mode+CASE_OFFSET <> MOVE2 THEN													//all the time of MOVE1 check if user dont change Move type
				MpAxisBasic_01.Stop := TRUE;																//if true stop the action
				motion_axis01_state := STANDSTILL;															//and change case to standstill
			END_IF;
			
			CASE Axis01_Move2 OF																			//Internal case for Move2 directions
				INIT_DIR:																					//Initial case are executed just once after move change
					brsstrcpy(ADR(action_text),ADR('Move2'));												//Write action text into Axis info into Visu
					MpAxisBasic_01.Home := TRUE;															//For absolut movement is needed to set up home position 0
					AxisPar01.Acceleration :=	ACLRTN_MOVE2;												//Acceleration is set up for very fast acceleration
					AxisPar01.Deceleration :=	DCLRTN_MOVE2;												//Deceleration is set up for very fast stop
					AxisPar01.Velocity :=		VLCTY_MOVE2;												//Full speed value
					IF MpAxisBasic_01.IsHomed AND MpAxisBasic_01.Position = 0 THEN 							//When Acopos is homed and position is 0 case can be changed to DIR1
						MpAxisBasic_01.Home := FALSE;														//reset Home for next using
						Axis01_Move2 := DIR1;																//DIR1
					END_IF;

				DIR1:
					AxisPar01.Position := 1000;																//Position are dynamically changed 
					MpAxisBasic_01.MoveAbsolute := TRUE;													//Change to this position
					IF MpAxisBasic_01.Position = 1000 THEN													//If position is reached stop movement and change state
						MpAxisBasic_01.MoveAbsolute := FALSE;												//stop movement
						Axis01_Move2 := DIR2;																//Direction 2
					END_IF;

				DIR2:
					AxisPar01.Position := 500;																//Position are dynamically changed 
					MpAxisBasic_01.MoveAbsolute := TRUE;													//Change to this position
					IF MpAxisBasic_01.Position = 500 THEN													//If position is reached stop movement and change state
						MpAxisBasic_01.MoveAbsolute := FALSE;												//stop movement
						Axis01_Move2 := DIR3;																//Direction 3
					END_IF;

				DIR3:
					AxisPar01.Position := 1500;																//Position are dynamically changed 
					MpAxisBasic_01.MoveAbsolute := TRUE;													//Change to this position
					IF MpAxisBasic_01.Position = 1500 THEN													//If position is reached stop movement and change state
						MpAxisBasic_01.MoveAbsolute := FALSE;												//stop movement
						Axis01_Move2 := DIR4;																//Direction 4
					END_IF;

				DIR4:
					AxisPar01.Position := 0;																//Position are dynamically changed
					MpAxisBasic_01.MoveAbsolute := TRUE;													//Change to this position
					IF MpAxisBasic_01.Position = 0 THEN														//If position is reached stop movement and change state
						MpAxisBasic_01.MoveAbsolute := FALSE;												//stop movement
						Axis01_Move2 := DIR1;																//Direction 1
					END_IF;
			END_CASE;
		(*---------------------------------------------MOVE3----------------------------------------------*)
		MOVE3:																								//In this movement motor do nothing, is used for example for autotuning
			brsstrcpy(ADR(action_text),ADR('Move3'));														//Write action text into Axis info into Visu
			motion_axis01_state := Axis01_Move_Mode+CASE_OFFSET;											//and change case to standstill

		ERROR:																								//Show most common error
			MpAxisBasic_01.JogPositive:= FALSE;																//Reset JogPositive
			MpAxisBasic_01.JogNegative:= FALSE;																//Reset JogNegative
			MpAxisBasic_01.Home := FALSE;																	//Reset Home
			MpAxisBasic_01.MoveAbsolute := FALSE;															//Reset MoveAbsolute
			Axis01_Error := MpAxisBasic_01.Info.Diag.Internal.Code;											//Save Error number into Case variable
			
			CASE Axis01_Error OF
				NO_ERROR:																					//If Error is 0(no error) change state do disconnected
					brsstrcpy(ADR(action_text),ADR('NO ERROR'));											//Write action text into Axis info into Visu
					motion_axis01_state := DISCONNECTED;
				
				LAG_ERROR:																					//lag error is needed to reset from Visu
					brsstrcpy(ADR(action_text),ADR('LAG ERROR!'));											//Write action text into Axis info into Visu
				
				HOMING_ERROR:																				//Homing error, needed to reset from Visu
					brsstrcpy(ADR(action_text),ADR('HOMING ERROR!'));										//Write action text into Axis info into Visu
				
				BAD_REQUEST:																				//Bad request, needed to reset from Visu
					brsstrcpy(ADR(action_text),ADR('BAD REQUEST!'));										//Write action text into Axis info into Visu
				
				UNPLUGGED:																					//When ACOPOS is unplugged, error is resolved automatically
					brsstrcpy(ADR(action_text),ADR('DISCONNECTED'));										//Write action text into Axis info into Visu
					MpAxisBasic_01.ErrorReset := TRUE;														//Error reset are true
					IF MpAxisBasic_01.Error = FALSE THEN													//When is Error solved
						MpAxisBasic_01.ErrorReset := FALSE;													//reset ErrorReset
						motion_axis01_state := DISCONNECTED;												//Change state do Disconnected
					END_IF;

				COMMUNICATION_LOST:																			//When ACOPOS lost comunication
					brsstrcpy(ADR(action_text),ADR('DISCONNECTED'));										//Write action text into Axis info into Visu
					MpAxisBasic_01.ErrorReset := FALSE;
					motion_axis01_state := DISCONNECTED;													//Change state do Disconnected
				
				BREAKDOWN:																					//After plug ACOPOS it will call Error
					brsstrcpy(ADR(action_text),ADR('DISCONNECTED'));										//Write action text into Axis info into Visu
					MpAxisBasic_01.ErrorReset := TRUE;														//Error reset are true
					IF MpAxisBasic_01.Error = FALSE THEN													//When is Error solved
						MpAxisBasic_01.ErrorReset := FALSE;													//reset ErrorReset
					END_IF;
					motion_axis01_state := DISCONNECTED;													//Change state do Disconnected, but also when condition is not fulfiled fulfilled
				ELSE																						//If is not any common error write only Error: error number
					brsitoa(MpAxisBasic_01.Info.Diag.Internal.Code,ADR(action_text_tmp));					//change number to string
					brsstrcpy(ADR(action_text),ADR('ERROR: '));												
					brsstrcat(ADR(action_text),ADR(action_text_tmp));										//Connect Error and error number into one string
			END_CASE;
	END_CASE;
END_PROGRAM
