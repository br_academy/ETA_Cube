
PROGRAM _INIT

	gVisu.SV20_UserPage.MpUserXManagerUI_0.MpLink := ADR(gUserXLogin);
	gVisu.SV20_UserPage.MpUserXLogin_0(MpLink := ADR(gUserXLogin), Enable := TRUE);
	gVisu.SV20_UserPage.MpUserXLoginUI_0(MpLink := ADR(gUserXLogin), Enable := TRUE, UIConnect := ADR(gVisu.SV20_UserPage.MpUserXLoginUIConnect));
	gVisu.SV20_UserPage.MpUserXManagerUI_0(Enable := TRUE, UIConnect := ADR(gVisu.SV20_UserPage.MpUserXManagerUIConnect));
	
	wcsconv(ADR(gVisu.SV20_UserPage.MpUserXLoginUIConnect.Login.UserName),ADR('Servis'),U8toUC);
	wcsconv(ADR(gVisu.SV20_UserPage.MpUserXLoginUIConnect.Login.Password),ADR('Servis'),U8toUC);
	gVisu.SV20_UserPage.MpUserXLoginUIConnect.Login.Login := TRUE;
		
END_PROGRAM

PROGRAM _CYCLIC
	
		
	FOR i:=0 TO gMAX_CLIENTS DO
		IF strcmp(ADR(gVisu.ClientInfo[i].currentPageId),ADR('SV20_UserPage')) = 0 THEN
			gVisu.SV20_UserPage.slotIdRights := gVisu.ClientInfo[i].slotId;
		END_IF;
	END_FOR;
	//Converts mpUserX listBox type to mappView listBox type
	IF memcmp(ADR(userList_tmp),ADR(gVisu.SV20_UserPage.MpUserXManagerUIConnect.User.List.UserNames),SIZEOF(userList_tmp)) <>0 THEN	
		memcpy(ADR(userList_tmp),ADR(gVisu.SV20_UserPage.MpUserXManagerUIConnect.User.List.UserNames),SIZEOF(userList_tmp));
		memset(ADR(gVisu.SV20_UserPage.userList),0,SIZEOF(gVisu.SV20_UserPage.userList));
		FOR i := 0 TO 19 DO
			IF (wcscmp(ADR(gVisu.SV20_UserPage.MpUserXManagerUIConnect.User.List.UserNames[i]),ADR(WSTRING_EMPTY)) <> 0) THEN
				IF ((wcscmp(ADR(gVisu.SV20_UserPage.MpUserXManagerUIConnect.User.List.UserNames[i]),ADR(WSTRING_ANONYMOUS)) <> 0) AND (wcscmp(ADR(gVisu.SV20_UserPage.MpUserXManagerUIConnect.User.List.UserNames[i]),ADR(WSTRING_SERVIS)) <> 0)) THEN
					wcscpy(ADR(userNameTmp),ADR(WSTRING_VALUE));
					itoa(i,ADR(strIndex));
					wcsconv(ADR(WstrIndex),ADR(strIndex),U8toUC);
					wcscat(ADR(userNameTmp),ADR(WstrIndex));
					wcscat(ADR(userNameTmp),ADR(WSTRING_TEXT));
					wcscat(ADR(userNameTmp),ADR(gVisu.SV20_UserPage.MpUserXManagerUIConnect.User.List.UserNames[i]));	
					wcscat(ADR(userNameTmp),ADR(WSTRING_ENDING));
					wcscpy(ADR(gVisu.SV20_UserPage.userList[i]),ADR(userNameTmp));
				END_IF;
			ELSE EXIT;
			END_IF;
		END_FOR
	END_IF;
	
	FOR i := 1 TO 9 DO
		IF (wcscmp(ADR(gVisu.SV20_UserPage.MpUserXManagerUIConnect.Role.List.Names[i]),ADR(WSTRING_EMPTY)) = 0) THEN
			EXIT;
		ELSIF (wcscmp(ADR(gVisu.SV20_UserPage.MpUserXManagerUIConnect.Role.List.Names[i]),ADR(WSTRING_EVERYONE)) <> 0) THEN
			wcscpy(ADR(gVisu.SV20_UserPage.roleDropDown[i]),ADR(WSTRING_VALUE));
			itoa(i,ADR(strIndex));
			wcsconv(ADR(WstrIndex),ADR(strIndex),U8toUC);
			wcscat(ADR(gVisu.SV20_UserPage.roleDropDown[i]),ADR(WstrIndex));
			wcscat(ADR(gVisu.SV20_UserPage.roleDropDown[i]),(ADR(WSTRING_TEXT)));
			wcscat(ADR(gVisu.SV20_UserPage.roleDropDown[i]),(ADR(gVisu.SV20_UserPage.MpUserXManagerUIConnect.Role.List.Names[i])));
			wcscat(ADR(gVisu.SV20_UserPage.roleDropDown[i]),(ADR(WSTRING_ENDING)));
		END_IF;
	END_FOR;
	
	IF gVisu.SV20_UserPage.MpUserXManagerUIConnect.MessageBox.ErrorNumber <> ERR_OK THEN
		gVisu.SV20_UserPage.ErrorMessage := '$$IAT/UsrMgmt.Dialog.Error.';
		itoa(gVisu.SV20_UserPage.MpUserXManagerUIConnect.MessageBox.ErrorNumber,ADR(strIndex));
		gVisu.SV20_UserPage.ErrorMessage := CONCAT(gVisu.SV20_UserPage.ErrorMessage, strIndex);
	END_IF;

	
	gVisu.SV20_UserPage.MpUserXLogin_0(MpLink := ADR(gUserXLogin), Enable := TRUE);
	gVisu.SV20_UserPage.MpUserXLoginUI_0(MpLink := ADR(gUserXLogin), Enable := TRUE, UIConnect := ADR(gVisu.SV20_UserPage.MpUserXLoginUIConnect));
	gVisu.SV20_UserPage.MpUserXManagerUI_0(Enable := TRUE, UIConnect := ADR(gVisu.SV20_UserPage.MpUserXManagerUIConnect));

END_PROGRAM