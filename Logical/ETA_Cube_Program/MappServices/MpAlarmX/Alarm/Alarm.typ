
TYPE
	GraphicDebuggerInput_type : 	STRUCT 
		mpLinkIndex : UINT := 0;
		enable : UINT := 1;
		errorReset : UINT := 0;
		sSeverity : STRING[80];
		sID : STRING[80];
	END_STRUCT;
END_TYPE
