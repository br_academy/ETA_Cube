
PROGRAM _INIT
	//Set MpAlarmXCore FB parameters
	MpAlarmXCore_0.MpLink := ADR(gAlarmXCore);
	MpAlarmXCore_0.Enable := TRUE;	
	//Set MpAlarmXListUI FB parameters
	MpAlarmXListUI_0.MpLink := ADR(gAlarmXCore); 
	MpAlarmXListUI_0.Enable := TRUE; 
	MpAlarmXListUI_0.UIConnect := ADR(MpAlarmXListUIConnect);	
	//Set MpAlarmXHistory FB parameters
	MpAlarmXHistory_0.MpLink := ADR(gAlarmXHistory); 
	MpAlarmXHistory_0.Enable := TRUE; 
	MpAlarmXHistory_0.DeviceName := ADR('mappDir');	
END_PROGRAM

PROGRAM _CYCLIC
	//Call FBs
	MpAlarmXCore_0();
	MpAlarmXListUI_0();
	MpAlarmXHistory_0();
		
   (***************************************************************************************************************
	*                  Implementation logic of Actual alarms + Alarm history tabs on AlarmContent                 *
	***************************************************************************************************************)

	FOR i:=0 TO (HwConfigInfo.Parameter.NrEntries) DO
		IF (HwConfigInfo.Status.StrConfiguredHW[i] <> HwConfigInfo.Status.StrPluggedHW[i]) THEN
			IF ((MpAxisBasic_01.Info.BootState = mpAXIS_BLP_NETWORK_INACTIVE) AND (MpAxisBasic_01.Error = TRUE)) THEN
				isPluggedAcopos1 := FALSE;
				AlarmWarningIndex := 1;
			ELSE
				isPluggedAcopos1 := TRUE;
				AlarmWarningIndex := 0;
			END_IF;
			IF ((MpAxisBasic_02.Info.BootState = mpAXIS_BLP_NETWORK_INACTIVE) AND (MpAxisBasic_02.Error = TRUE)) THEN
				isPluggedAcopos2 := FALSE;
				AlarmWarningIndex := 1;
			ELSE
				isPluggedAcopos2 := TRUE;
				AlarmWarningIndex := 0;
			END_IF;
			IF HwConfigInfo.Status.StrPluggedHW[i] <> 'X20PS9400a' THEN
				AlarmName := HwConfigInfo.Status.StrConfiguredHW[i];
				invokeHwAlarmDisabledIndex[i] := FALSE;
				MpAlarmXSet(gAlarmXCore,AlarmName);
			END_IF
		ELSE
			invokeHwAlarmDisabledIndex[i] := TRUE;
		END_IF			
	END_FOR;
	
	IF (SimuPlatTemp > 40.0) THEN
		AlarmName := 'HighTemp';
		MpAlarmXSet(gAlarmXCore,AlarmName);
	END_IF		
				
	FOR i:=0 TO 49 DO																			
		IF (MpAlarmXListUIConnect.AlarmList.StateActive[i] = TRUE) THEN
			uAct := uAct + 1;
			IF (MpAlarmXListUIConnect.AlarmList.StateAcknowledged[i] = TRUE) THEN
				uAck := uAck + 1;
				IF (ResetAlarm) THEN
					AlarmName := MpAlarmXListUIConnect.AlarmList.Name[i];
					MpAlarmXReset(gAlarmXCore,AlarmName);
					ResetAlarm := FALSE;
				END_IF
			END_IF
		END_IF
	END_FOR;
	
	IF (uAct <> uAck) THEN
		AlarmWarningIndex := 1;		
	ELSE
		AlarmWarningIndex := 0;
	END_IF
	
	uAct := 0;
	uAck := 0;
	
   (***************************************************************************************************************
	*                          Implementation logic of Invoke alarm tab on AlarmContent                           *
	***************************************************************************************************************)
	IF (invokeHwAlarm) THEN
		invokeHwAlarm := FALSE;
		FOR i:=0 TO 49 DO
			IF (invokeHwAlarmIndex[i]) THEN
				AlarmName := HwConfigInfo.Status.StrPluggedHW[i];
				MpAlarmXSet(gAlarmXCore,AlarmName);
			END_IF
		END_FOR;
	END_IF
	
	(***************************************************************************************************************
	*                          Implementation logic of Graphic debugger alarm tab on AlarmContent                           *
	***************************************************************************************************************)
	IF(GraphicDebuggerInput.mpLinkIndex = 0) THEN
		MpAlarmXCore_0.MpLink := ADR(gAlarmXCore);
	ELSE
		MpAlarmXCore_0.MpLink := 0;
	END_IF;
	
	MpAlarmXCore_0.Enable := UINT_TO_BOOL(GraphicDebuggerInput.enable);
	MpAlarmXCore_0.ErrorReset := UINT_TO_BOOL(GraphicDebuggerInput.errorReset);
	
	CASE MpAlarmXCore_0.Info.Diag.StatusID.Severity OF
		0: GraphicDebuggerInput.sSeverity := 'mpCOM_SEV_SUCCESS';
		1: GraphicDebuggerInput.sSeverity := 'mpCOM_SEV_INFORMATIONAL';
		2: GraphicDebuggerInput.sSeverity := 'mpCOM_SEV_WARNING';
		3: GraphicDebuggerInput.sSeverity := 'mpCOM_SEV_ERROR';
	END_CASE;
	
	CASE MpAlarmXCore_0.Info.Diag.StatusID.ID OF
		0: GraphicDebuggerInput.sID := LIST_OF_MPALARMXCORE_INFO_CODES[0];
		-1064239103: GraphicDebuggerInput.sID := LIST_OF_MPALARMXCORE_INFO_CODES[1];
 	    -1064239102: GraphicDebuggerInput.sID := LIST_OF_MPALARMXCORE_INFO_CODES[2];
		-1064239101: GraphicDebuggerInput.sID := LIST_OF_MPALARMXCORE_INFO_CODES[3];
		-1064239100: GraphicDebuggerInput.sID := LIST_OF_MPALARMXCORE_INFO_CODES[4];
		-1064239099: GraphicDebuggerInput.sID := LIST_OF_MPALARMXCORE_INFO_CODES[5];
		-1064239098: GraphicDebuggerInput.sID := LIST_OF_MPALARMXCORE_INFO_CODES[6];
		-1064239096: GraphicDebuggerInput.sID := LIST_OF_MPALARMXCORE_INFO_CODES[7];
		-1064239091: GraphicDebuggerInput.sID := LIST_OF_MPALARMXCORE_INFO_CODES[8];
		-1064116224: GraphicDebuggerInput.sID := LIST_OF_MPALARMXCORE_INFO_CODES[9];
		-1064116223: GraphicDebuggerInput.sID := LIST_OF_MPALARMXCORE_INFO_CODES[10];
		-2137858045: GraphicDebuggerInput.sID := LIST_OF_MPALARMXCORE_INFO_CODES[11];
		-1064116220: GraphicDebuggerInput.sID := LIST_OF_MPALARMXCORE_INFO_CODES[12];
		-1064116219: GraphicDebuggerInput.sID := LIST_OF_MPALARMXCORE_INFO_CODES[13];
		-1064116218: GraphicDebuggerInput.sID := LIST_OF_MPALARMXCORE_INFO_CODES[14];
		-1064116217: GraphicDebuggerInput.sID := LIST_OF_MPALARMXCORE_INFO_CODES[15];
		1083367432: GraphicDebuggerInput.sID := LIST_OF_MPALARMXCORE_INFO_CODES[16];
		-1064116215: GraphicDebuggerInput.sID := LIST_OF_MPALARMXCORE_INFO_CODES[17];
		-1064116214: GraphicDebuggerInput.sID := LIST_OF_MPALARMXCORE_INFO_CODES[18];
		-1064116213: GraphicDebuggerInput.sID := LIST_OF_MPALARMXCORE_INFO_CODES[19];
		-1064116212: GraphicDebuggerInput.sID := LIST_OF_MPALARMXCORE_INFO_CODES[20];
		-1064116211: GraphicDebuggerInput.sID := LIST_OF_MPALARMXCORE_INFO_CODES[21];
	END_CASE;
	
END_PROGRAM

PROGRAM _EXIT
	MpAlarmXCore_0(Enable := FALSE);
	MpAlarmXListUI_0(Enable := FALSE);
	MpAlarmXHistory_0(Enable := FALSE);
END_PROGRAM


