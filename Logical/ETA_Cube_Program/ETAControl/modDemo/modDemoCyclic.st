(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * PROGRAM: modDemo
 * File: modDemoCyclic.st
 * Author: Bubenik
 * Created: February 26, 2016
 ********************************************************************
 * Implementation OF PROGRAM modDemo
 ********************************************************************)

PROGRAM _CYCLIC
	
	IF demoOut1 THEN																		//enables demo mode
		
		IF InitDemo THEN																	//initialize conditions after switch to demo
			ButtonStep := BUTTON_INIT;														
			anlog_out_tmp01 := gHMIsigSim.EXTAnalogOut01;
			anlog_out_tmp02 := gHMIsigSim.EXTAnalogOut02;
			gEXTIOModule.AOut.AnalogDisplay01 :=-32700;
			gEXTIOModule.AOut.AnalogDisplay02 :=0;
			InitDemo := FALSE;																//after initialize turn of variable
			Allow_rights := 1;																//Demo log off verified person by RFID
		END_IF;

		TON_SwitchButton(IN := TRUE, PT := timeSwitchButton);								//timer to switch DOleds

		CASE ButtonStep OF 																	//switch to DOleds

			BUTTON_INIT:																	//init condition for first circuit of leds
				FOR i:=1 TO 8 DO															//For loop for DO LEDs 
					gX20IOModule.DOut.DOled[i]:=0;											//all DO LEDs turn off
				END_FOR;
				j:=1;																		//offset for first DO LED
				ButtonStep:= BUTTON_CYCLIC_LIGHT;											//change to cycle of DO LEDs
			
			BUTTON_CYCLIC_LIGHT:
				IF TON_SwitchButton.Q THEN													//Explain of BUTTON_CYCLIC_LIGHT
					IF j>0 AND j<5 THEN														//Num:	1	2	3	4
						gX20IOModule.DOut.DOled[j] := NOT gX20IOModule.DOut.DOled[j];		//LEDs:	o->	o->	o->	o->
						j:=j+1;																//LEDs:	o<-	o<-	o<-	o<-
						IF j=5 THEN															//Num:	5	6	7	8
							j:=8;
						END_IF;
					
					ELSIF j>4 AND j<9 THEN
						gX20IOModule.DOut.DOled[j] := NOT gX20IOModule.DOut.DOled[j];
						j:=j-1;
						IF j=4 THEN
							j:=1;
						END_IF;						
					END_IF;	
					TON_SwitchButton(IN := FALSE);
				END_IF;
		END_CASE

		
		CASE LampDemoStep OF																

			SWITCH_INIT_STEP:																//Initial phase after change of mode
				TON_SwitchLampDemo(IN := FALSE, PT := 60);									//reset all working variables
				LampDemoStep := SWICH_FIRST_STEP;
				gHMISensorColor.sXPColorLamp01 := "LightButtonGreyStyle";
				gHMISensorColor.sXPColorLamp02 := "LightButtonGreyStyle";
				gHMISensorColor.sXPColorLamp03 := "LightButtonGreyStyle";
				gHMISensorColor.sXPColorLamp04 := "LightButtonGreyStyle";
				gHMISensorColor.uXPColorLamp01 := 0;
				gHMISensorColor.uXPColorLamp02 := 0;
				gHMISensorColor.uXPColorLamp03 := 0;
				gHMISensorColor.uXPColorLamp04 := 0;
				gXPIOModule.DOut.DOledRed01 := FALSE;
				gXPIOModule.DOut.DOledRed02 := FALSE;
				gXPIOModule.DOut.DOledRed03 := FALSE;
				gXPIOModule.DOut.DOledRed04 := FALSE;
				gXPIOModule.DOut.DOledYellow01 := FALSE;
				gXPIOModule.DOut.DOledYellow02 := FALSE;
				gXPIOModule.DOut.DOledYellow03 := FALSE;
				gXPIOModule.DOut.DOledYellow04 := FALSE;
				gXPIOModule.DOut.DOledGreen01 := FALSE;
				gXPIOModule.DOut.DOledGreen02 := FALSE;
				gXPIOModule.DOut.DOledGreen03 := FALSE;
				gXPIOModule.DOut.DOledGreen04 := FALSE;

			
			SWICH_FIRST_STEP:
				
				TON_SwitchLampDemo(IN := TRUE, PT := timeSwitchLamp);							//TON for change the phase
				CASE LampInStep OF																//Case that change XPLamp lights
					
					SWITCH_FIRST:																
						gHMISensorColor.sXPColorLamp01 := "LightButtonRedStyle";				//Turn on Red in Visu on XP button 1
						gHMISensorColor.uXPColorLamp01 := 1;
						gXPIOModule.DOut.DOledYellow01 := FALSE;								//Turn off Yellow on XPLamp of button 1
						gXPIOModule.DOut.DOledRed01 := TRUE;									//Turn on Red on XPLamp of button 1
						IF TON_SwitchLampDemo.Q THEN											//Timer to change phase
							LampInStep := SWITCH_SECOND;										//Change phase
							TON_SwitchLampDemo(IN := FALSE);									//Reset timer
						END_IF;
						
					SWITCH_SECOND:																
						gHMISensorColor.sXPColorLamp02 := "LightButtonRedStyle";				//Turn on Red in Visu on XP button 2
						gHMISensorColor.uXPColorLamp02 := 1;
						gXPIOModule.DOut.DOledYellow02 := FALSE;								//Turn off Yellow on XPLamp of button 2
						gXPIOModule.DOut.DOledRed02 := TRUE;									//Turn on Red on XPLamp of button 2
						IF TON_SwitchLampDemo.Q THEN											//Timer to change phase
							LampInStep := SWITCH_THIRD;											//Change phase
							TON_SwitchLampDemo(IN := FALSE);									//Reset timer
						END_IF;
					
					SWITCH_THIRD:
						gHMISensorColor.sXPColorLamp04 := "LightButtonRedStyle";										//Turn on Red in Visu on XP button 4
						gHMISensorColor.uXPColorLamp04 := 1;
						gXPIOModule.DOut.DOledYellow04 := FALSE;								//Turn off Yellow on XPLamp of button 4
						gXPIOModule.DOut.DOledRed04 := TRUE;									//Turn on Red on XPLamp of button 4
						IF TON_SwitchLampDemo.Q THEN											//Timer to change phase
							LampInStep := SWITCH_FOURTH;										//Change phase
							TON_SwitchLampDemo(IN := FALSE);									//Reset timer
						END_IF;
					
					SWITCH_FOURTH:
						gHMISensorColor.sXPColorLamp03 := "LightButtonRedStyle";										//Turn on Red in Visu on XP button 3
						gHMISensorColor.uXPColorLamp03 := 1;
						gXPIOModule.DOut.DOledYellow03 := FALSE;								//Turn off Yellow on XPLamp of button 3
						gXPIOModule.DOut.DOledRed03 := TRUE;									//Turn on Red on XPLamp of button 3
						IF TON_SwitchLampDemo.Q THEN											//Timer to change phase
							LampInStep := SWITCH_FIRST;											//Change phase
							LampDemoStep := SWICH_SECOND_STEP;									//Change phase of light color
							TON_SwitchLampDemo(IN := FALSE);									//Reset timer
						END_IF;		
				END_CASE;
			
			SWICH_SECOND_STEP:
				
				TON_SwitchLampDemo(IN := TRUE, PT := timeSwitchLamp);							//TON for change the phase
				CASE LampInStep OF																//Case that change XPLamp lights
					SWITCH_FIRST:
						gHMISensorColor.sXPColorLamp01 := "LightButtonYellowStyle";										//Turn on Green in Visu on XP button 1
						gHMISensorColor.uXPColorLamp01 := 2;
						gXPIOModule.DOut.DOledRed01 := FALSE;									//Turn off Red on XPLamp of button 1
						gXPIOModule.DOut.DOledGreen01 := TRUE;									//Turn on Green on XPLamp of button 1
						IF TON_SwitchLampDemo.Q THEN											//Timer to change phase
							LampInStep := SWITCH_SECOND;										//Change phase
							TON_SwitchLampDemo(IN := FALSE);									//Reset timer
						END_IF;
					
					SWITCH_SECOND:
						gHMISensorColor.sXPColorLamp02 := "LightButtonYellowStyle";										//Turn on Green in Visu on XP button 2
						gHMISensorColor.uXPColorLamp02 := 2;
						gXPIOModule.DOut.DOledRed02 := FALSE;									//Turn off Red on XPLamp of button 2
						gXPIOModule.DOut.DOledGreen02 := TRUE;									//Turn on Green on XPLamp of button 2
						IF TON_SwitchLampDemo.Q THEN											//Timer to change phase
							LampInStep := SWITCH_THIRD;											//Change phase
							TON_SwitchLampDemo(IN := FALSE);									//Reset timer
						END_IF;
					
					SWITCH_THIRD:
						gHMISensorColor.sXPColorLamp04 := "LightButtonYellowStyle";										//Turn on Green in Visu on XP button 4
						gHMISensorColor.uXPColorLamp04 := 2;
						gXPIOModule.DOut.DOledRed04 := FALSE;									//Turn off Red on XPLamp of button 4
						gXPIOModule.DOut.DOledGreen04 := TRUE;									//Turn on Green on XPLamp of button 4
						IF TON_SwitchLampDemo.Q THEN											//Timer to change phase
							LampInStep := SWITCH_FOURTH;										//Change phase
							TON_SwitchLampDemo(IN := FALSE);									//Reset timer
						END_IF;
					
					SWITCH_FOURTH:
						gHMISensorColor.sXPColorLamp03 := "LightButtonYellowStyle";										//Turn on Green in Visu on XP button 3
						gHMISensorColor.uXPColorLamp03 := 2;
						gXPIOModule.DOut.DOledRed03 := FALSE;									//Turn off Red on XPLamp of button 3
						gXPIOModule.DOut.DOledGreen03 := TRUE;									//Turn on Green on XPLamp of button 3
						IF TON_SwitchLampDemo.Q THEN											//Timer to change phase
							LampInStep := SWITCH_FIRST;											//Change phase
							LampDemoStep := SWICH_THIRD_STEP;									//Change phase of light color
							TON_SwitchLampDemo(IN := FALSE);									//Reset timer
						END_IF;		
				END_CASE;
			
			SWICH_THIRD_STEP:
			
				TON_SwitchLampDemo(IN := TRUE, PT := timeSwitchLamp);							//TON for change the phase
				CASE LampInStep OF																//Case that change XPLamp lights
					SWITCH_FIRST:
						gHMISensorColor.sXPColorLamp01 := "LightButtonGreenStyle";										//Turn on Yellow in Visu on XP button 1
						gHMISensorColor.uXPColorLamp01 := 3;
						gXPIOModule.DOut.DOledGreen01 := FALSE;									//Turn off Green on XPLamp of button 1
						gXPIOModule.DOut.DOledYellow01 := TRUE;									//Turn on Yellow on XPLamp of button 1
						IF TON_SwitchLampDemo.Q THEN											//Timer to change phase
							LampInStep := SWITCH_SECOND;										//Change phase
							TON_SwitchLampDemo(IN := FALSE);									//Reset timer
						END_IF;
					
					SWITCH_SECOND:
						gHMISensorColor.sXPColorLamp02 := "LightButtonGreenStyle";										//Turn on Yellow in Visu on XP button 2
						gHMISensorColor.uXPColorLamp02 := 3;
						gXPIOModule.DOut.DOledGreen02 := FALSE;									//Turn off Green on XPLamp of button 2
						gXPIOModule.DOut.DOledYellow02 := TRUE;									//Turn on Yellow on XPLamp of button 2
						IF TON_SwitchLampDemo.Q THEN											//Timer to change phase
							LampInStep := SWITCH_THIRD;											//Change phase
							TON_SwitchLampDemo(IN := FALSE);									//Reset timer
						END_IF;
					
					SWITCH_THIRD:
						gHMISensorColor.sXPColorLamp04 := "LightButtonGreenStyle";										//Turn on Yellow in Visu on XP button 4
						gHMISensorColor.uXPColorLamp04 := 3;
						gXPIOModule.DOut.DOledGreen04 := FALSE;									//Turn off Green on XPLamp of button 4
						gXPIOModule.DOut.DOledYellow04 := TRUE;									//Turn on Yellow on XPLamp of button 4
						IF TON_SwitchLampDemo.Q THEN											//Timer to change phase
							LampInStep := SWITCH_FOURTH;										//Change phase
							TON_SwitchLampDemo(IN := FALSE);									//Reset timer
						END_IF;
					
					SWITCH_FOURTH:
						gHMISensorColor.sXPColorLamp03 := "LightButtonGreenStyle";										//Turn on Yellow in Visu on XP button 3
						gHMISensorColor.uXPColorLamp03 := 3;
						gXPIOModule.DOut.DOledGreen03 := FALSE;									//Turn off Green on XPLamp of button 3
						gXPIOModule.DOut.DOledYellow03 := TRUE;									//Turn on Yellow on XPLamp of button 3
						IF TON_SwitchLampDemo.Q THEN											//Timer to change phase
							LampInStep := SWITCH_FIRST;											//Change phase
							LampDemoStep := SWICH_FIRST_STEP;									//Change phase of light color
							TON_SwitchLampDemo(IN := FALSE);									//Reset timer
						END_IF;		
				END_CASE;
			END_CASE;
		
		gEXTIOModule.AOut.AnalogDisplay01:= gEXTIOModule.AOut.AnalogDisplay01+327;					//Rising and falling numbers on the segment display
		gHMIsigSim.EXTAnalogOut01:=INT_TO_REAL(gEXTIOModule.AOut.AnalogDisplay01)*INT_TO_V;
		gEXTIOModule.AOut.AnalogDisplay02:= gEXTIOModule.AOut.AnalogDisplay02+327;
		gHMIsigSim.EXTAnalogOut02:=INT_TO_REAL(gEXTIOModule.AOut.AnalogDisplay02)*INT_TO_V;
		
		FOR i:=0 TO (gVisu.userValidCount) DO
			IF Infotainment.Automatic.enable[i] THEN
				IF ((gVisu.ClientInfo[i].currentPageId = 'B01_ControlPage') AND (NOT Infotainment.Automatic.Dialog.Timer.TON_1[i].Q)) THEN
					Infotainment.Automatic.Dialog.Timer.TON_1[i](IN := TRUE,PT := (DT_TO_TIME(Infotainment.Automatic.Dialog.tOpenDlg))*1000);
				ELSE 
					Infotainment.Automatic.Dialog.Timer.TON_1[i](IN := FALSE);
					Infotainment.Automatic.Dialog.Timer.TON_Delay[i](IN := FALSE);
					Infotainment.Automatic.Dialog.Timer.TON_2[i](IN := FALSE);
				END_IF;

				CASE Infotainment.Automatic.step[i] OF
					OPEN_DIALOG:
						IF (Infotainment.Automatic.Dialog.Timer.TON_1[i].Q) THEN
							Infotainment.Automatic.Dialog.Timer.TON_1[i](IN := FALSE);
							IF (Infotainment.Automatic.Dialog.video_pdfFileNum[i] > BANNERS_COUNT) THEN 
								Infotainment.Automatic.Dialog.video_pdfFileNum[i] := 0;
							END_IF;	
							Infotainment.Automatic.Dialog.video_pdfFileNum[i] := UDINT_TO_USINT(Infotainment.Automatic.Dialog.Timer.cycleNum[i] MOD 4);
							Infotainment.Automatic.Dialog.Timer.cycleNum[i] := Infotainment.Automatic.Dialog.Timer.cycleNum[i] + 1;
							Infotainment.Automatic.Dialog.videoWord[i] := VIDEO_INFOTAINMENT_LIST[Infotainment.Automatic.Dialog.video_pdfFileNum[i]]; 
							Infotainment.Automatic.Dialog.pdfWord[i] := PDF_INFOTAINMENT_LIST[Infotainment.Automatic.Dialog.video_pdfFileNum[i]];
							Infotainment.Automatic.Dialog.openDialog[i] := TRUE;
							Infotainment.Automatic.Banner.startCycle[i] := FALSE; 
							Infotainment.Automatic.Banner.Timer.TON_1[i](IN := FALSE); 
							Infotainment.Automatic.step[i] := 1;
						END_IF;
							
					SWITCH_VIDEO_TAB:		
						IF (NOT Infotainment.Automatic.Dialog.Timer.TON_Delay[i].Q) THEN
							Infotainment.Automatic.Dialog.Timer.TON_Delay[i](IN := TRUE,PT := T#1s);
						END_IF;
						
						IF (Infotainment.Automatic.Dialog.Timer.TON_Delay[i].Q) THEN
							Infotainment.Automatic.Dialog.Timer.TON_Delay[i](IN := FALSE);
							Infotainment.Automatic.Dialog.openDialog[i] := FALSE;
							Infotainment.Automatic.step[i] := 2;
						END_IF;
						
					PLAY_VIDEO:
						IF (NOT Infotainment.Automatic.Dialog.Timer.TON_Delay[i].Q) THEN
							Infotainment.Automatic.Dialog.Timer.TON_Delay[i](IN := TRUE,PT := T#1s);
						END_IF;				
							IF (Infotainment.Automatic.Dialog.Timer.TON_Delay[i].Q) OR (Infotainment.Automatic.Dialog.doneTON_Delay[i]) THEN
							Infotainment.Automatic.Dialog.Timer.TON_Delay[i](IN := FALSE);	
							IF (NOT Infotainment.Automatic.Dialog.Timer.TON_2[i].Q) THEN
								Infotainment.Automatic.Dialog.doneTON_Delay[i] := TRUE;
								Infotainment.Automatic.Dialog.TabControl.isTabVideoControl[i] := FALSE; 
								Infotainment.Automatic.Dialog.TabControl.videoStart[i] := TRUE;
								Infotainment.Automatic.Dialog.Timer.TON_2[i](IN := TRUE,PT := (DT_TO_TIME(Infotainment.Automatic.Dialog.tPlayVideo))*1000);
							END_IF;
						END_IF;
						
						IF (Infotainment.Automatic.Dialog.Timer.TON_2[i].Q) THEN
							Infotainment.Automatic.Dialog.Timer.TON_2[i](IN := FALSE);
							Infotainment.Automatic.Dialog.doneTON_Delay[i] := FALSE;
							Infotainment.Automatic.Dialog.TabControl.switchPdfPage[i] := 1; 
							Infotainment.Automatic.step[i] := 3;
						END_IF;
						
					SWITCH_PDF_TAB:
						IF (NOT Infotainment.Automatic.Dialog.Timer.TON_Delay[i].Q) THEN
							Infotainment.Automatic.Dialog.TabControl.isTabPdfControl[i] := TRUE;
							Infotainment.Automatic.Dialog.TabControl.videoStart[i] := FALSE;
							Infotainment.Automatic.Dialog.Timer.TON_Delay[i](IN := TRUE,PT := T#2s);
						END_IF;
					
						IF (Infotainment.Automatic.Dialog.Timer.TON_Delay[i].Q) THEN
							Infotainment.Automatic.Dialog.Timer.TON_Delay[i](IN := FALSE);
							Infotainment.Automatic.Dialog.TabControl.isTabPdfControl[i] := FALSE;
							Infotainment.Automatic.step[i] := 4;
						END_IF;
					
					SCROLL_PAGES_PDF:
						IF (NOT Infotainment.Automatic.Dialog.Timer.TON_2[i].Q) THEN
							Infotainment.Automatic.Dialog.Timer.TON_2[i](IN := TRUE,PT := T#20s);
							IF(Infotainment.Automatic.Dialog.Timer.TON_2[i].Q)THEN
								Infotainment.Automatic.Dialog.Timer.TON_2[i](IN := FALSE);
								Infotainment.Automatic.step[i] := 5;
							ELSIF(Infotainment.Automatic.Dialog.Timer.TON_2[i].ET >= T#16s)THEN
								Infotainment.Automatic.Dialog.TabControl.switchPdfPage[i] := 13;
							ELSIF(Infotainment.Automatic.Dialog.Timer.TON_2[i].ET >= T#12s)THEN
								Infotainment.Automatic.Dialog.TabControl.switchPdfPage[i] := 6;
							ELSIF(Infotainment.Automatic.Dialog.Timer.TON_2[i].ET >= T#8s)THEN
								Infotainment.Automatic.Dialog.TabControl.switchPdfPage[i] := 4;
							ELSIF (Infotainment.Automatic.Dialog.Timer.TON_2[i].ET >= T#4s) THEN
								Infotainment.Automatic.Dialog.TabControl.switchPdfPage[i] := 3;
							ELSE
								Infotainment.Automatic.Dialog.TabControl.switchPdfPage[i] := 1;
							END_IF;
						END_IF;
										
					CLOSE_DIALOG:
						Infotainment.Automatic.Dialog.closeDialog[i] := TRUE;
						Infotainment.Automatic.Dialog.TabControl.isTabVideoControl[i] := TRUE;
						Infotainment.Automatic.Banner.startCycle[i] := TRUE; 
						Infotainment.Automatic.Banner.actualNum[i] := 0;					
						Infotainment.Automatic.Dialog.video_pdfFileNum[i] := Infotainment.Automatic.Dialog.video_pdfFileNum[i] + 1;	
						Infotainment.Automatic.step[i] := 0;
																	   														
				END_CASE;
				
				IF(Infotainment.Automatic.Banner.startCycle[i]) THEN
					IF (Infotainment.Automatic.Banner.actualNum[i] > BANNERS_COUNT) THEN
						Infotainment.Automatic.Banner.actualNum[i] := 0;
					END_IF;
					
					IF(NOT Infotainment.Automatic.Banner.Timer.TON_1[i].Q) THEN
						Infotainment.Automatic.Banner.controlWord[i] := BANNER_LIST[Infotainment.Automatic.Banner.actualNum[i]];
						Infotainment.Automatic.Banner.Timer.TON_1[i](IN := TRUE,PT := (UDINT_TO_TIME((((DT_TO_UDINT(Infotainment.Automatic.Dialog.tOpenDlg)) - ((DT_TO_UDINT(ALL_DELAY_SUM)) + (DT_TO_UDINT(Infotainment.Automatic.Dialog.tPlayVideo))))/BANNERS_NUM))*1000)); 
					END_IF;
					
					IF(Infotainment.Automatic.Banner.Timer.TON_1[i].Q) THEN
						Infotainment.Automatic.Banner.actualNum[i] := Infotainment.Automatic.Banner.actualNum[i] + 1;
						Infotainment.Automatic.Banner.controlWord[i] := BANNER_LIST[Infotainment.Automatic.Banner.actualNum[i]];
						Infotainment.Automatic.Banner.Timer.TON_1[i](IN := FALSE);
					END_IF;
				END_IF;
				
			ELSE		
				IF (Infotainment.Automatic.Banner.actualNum[i] > BANNERS_COUNT) THEN
					Infotainment.Automatic.Banner.actualNum[i] := 0;
				END_IF;
				
				IF(NOT Infotainment.Automatic.Banner.Timer.TON_1[i].Q) THEN
					Infotainment.Automatic.Banner.controlWord[i] := BANNER_LIST[Infotainment.Automatic.Banner.actualNum[i]];
					Infotainment.Automatic.Banner.Timer.TON_1[i](IN := TRUE,PT := (DT_TO_TIME(Infotainment.Automatic.Banner.tChangeBanner))*1000); 
				END_IF;
				
				IF(Infotainment.Automatic.Banner.Timer.TON_1[i].Q) THEN
					Infotainment.Automatic.Banner.actualNum[i] := Infotainment.Automatic.Banner.actualNum[i] + 1;
					Infotainment.Automatic.Banner.controlWord[i] := BANNER_LIST[Infotainment.Automatic.Banner.actualNum[i]];
					Infotainment.Automatic.Banner.Timer.TON_1[i](IN := FALSE);
				END_IF;
				
			END_IF;			
		END_FOR;
	END_IF;
	gVisu.userValidCount := 0;
END_PROGRAM
