(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * PROGRAM: modDemo
 * File: modDemo.typ
 * Author: Bubenik
 * Created: February 26, 2016
 ********************************************************************
 * Implementation OF PROGRAM modDemo
 ********************************************************************)

TYPE
	LampInStep_Enum : 
		( (*Change same color on all buttons*)
		SWITCH_FIRST,
		SWITCH_SECOND,
		SWITCH_THIRD,
		SWITCH_FOURTH
		);
	LampStepDemo_Enum : 
		( (*Change color*)
		SWITCH_INIT_STEP,
		SWICH_FIRST_STEP, (*Swtich on the First light*)
		SWICH_SECOND_STEP, (*Swtich on the Second light*)
		SWICH_THIRD_STEP, (*Swtich on the Third light*)
		SWITCH_FOURTH_STEP (*Swtich Off light*)
		);
	ButtonStep_Enum : 
		( (*Enum for DO switches CASE*)
		BUTTON_INIT,
		BUTTON_CYCLIC_LIGHT
		);
	InfotainmentStep_Enum : 
		(
		OPEN_DIALOG,
		SWITCH_VIDEO_TAB,
		PLAY_VIDEO,
		SWITCH_PDF_TAB,
		SCROLL_PAGES_PDF,
		CLOSE_DIALOG
		);
END_TYPE
