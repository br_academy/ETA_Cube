﻿<?xml version="1.0" encoding="utf-8"?>
<?AutomationStudio Version=4.2.3.159?>
<Program xmlns="http://br-automation.co.at/AS/Program">
  <Files>
    <File Description="Cyclic Demo mod program">modDemoCyclic.st</File>
    <File Description="Init Demo mode program">modDemoInit.st</File>
    <File Description="Local data types" Private="true">modDemo.typ</File>
    <File Description="Local variables" Private="true">modDemo.var</File>
  </Files>
</Program>