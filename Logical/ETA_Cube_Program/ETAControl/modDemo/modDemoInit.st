(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * PROGRAM: modDemo
 * File: modDemoInit.st
 * Author: Bubenik
 * Created: February 26, 2016
 ********************************************************************
 * Implementation OF PROGRAM modDemo
 ********************************************************************)

PROGRAM _INIT
	
	IF timeSwitchButton = 0 THEN				//timer for DO LEDs sequence
		timeSwitchButton := 50;	
	END_IF;
	
	IF timeSwitchLamp = 0 THEN					//timer for XP Lamps sequence
		timeSwitchLamp := 5;
	END_IF;
	
	Infotainment.Automatic.Dialog.tOpenDlg := DT#1970-01-01-00:03:00;
	Infotainment.Automatic.Dialog.tPlayVideo := DT#1970-01-01-00:00:30;
	Infotainment.Automatic.Banner.tChangeBanner := DT#1970-01-01-00:00:15;
		
END_PROGRAM