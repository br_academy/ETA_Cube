(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * PROGRAM: modSimul
 * File: modSimul.st
 * Author: Bubenik
 * Created: February 26, 2016
 ********************************************************************
 * Implementation OF PROGRAM modSimul
 ********************************************************************)

PROGRAM _CYCLIC
	
	IF simulOut1 THEN																		//enables simulation mode
		
		IF (InitSimul) THEN																	//Init conditions
			
			FOR i:=1 TO 8 DO																//reset all DI switches
				gHMIsigSim.X20DIswitch[i]:= FALSE;
			END_FOR;
			
			IF SoftButTouch <> 0 THEN														//turn on touched button after demo
				gHMIsigSim.X20DIswitch[SoftButTouch] := TRUE;
			END_IF
			
			SoftButTouch := 0;
			gHMISensorColor.sXPColorLamp01 := "LightButtonGreyStyle";
			gHMISensorColor.sXPColorLamp02 := "LightButtonGreyStyle";
			gHMISensorColor.sXPColorLamp03 := "LightButtonGreyStyle";
			gHMISensorColor.sXPColorLamp04 := "LightButtonGreyStyle";
			gHMISensorColor.uXPColorLamp01 := 0;
			gHMISensorColor.uXPColorLamp02 := 0;
			gHMISensorColor.uXPColorLamp03 := 0;
			gHMISensorColor.uXPColorLamp04 := 0;
			gXPIOModule.DOut.DOledRed01 := FALSE;
			gXPIOModule.DOut.DOledRed02 := FALSE;
			gXPIOModule.DOut.DOledRed03 := FALSE;
			gXPIOModule.DOut.DOledRed04 := FALSE;
			gXPIOModule.DOut.DOledYellow01 := FALSE;
			gXPIOModule.DOut.DOledYellow02 := FALSE;
			gXPIOModule.DOut.DOledYellow03 := FALSE;
			gXPIOModule.DOut.DOledYellow04 := FALSE;
			gXPIOModule.DOut.DOledGreen01 := FALSE;
			gXPIOModule.DOut.DOledGreen02 := FALSE;
			gXPIOModule.DOut.DOledGreen03 := FALSE;
			gXPIOModule.DOut.DOledGreen04 := FALSE;
			Lamp01Step := SWITCH_OFF;
			Lamp02Step := SWITCH_OFF;
			Lamp03Step := SWITCH_OFF;
			Lamp04Step := SWITCH_OFF;
			InitSimul:=FALSE;
		END_IF;
		
		FOR i:=1 TO 8 DO																	
			gX20IOModule.DOut.DOled[i]:= gHMIsigSim.X20DIswitch[i];							//get back DI settings before mode change		
			gHMISensorColor.X20DIswitch[i] := BOOL_TO_USINT(gHMIsigSim.X20DIswitch[i])*2;	//change to yellow colour every forced button in visu
			IF gX20IOModule.DInp.DIswitch[i] <> gHMIsigSim.X20DIswitch[i] THEN
				isSimulation[i] := TRUE;
			ELSE 
				isSimulation[i] := FALSE;
			END_IF;				
		END_FOR;
				
		TON_SwitchLamp01(IN := TRUE, PT := 10);												//TON for first XP button
		
		CASE Lamp01Step OF																	//Lamp 1 step case
			
			SWITCH_FIRST_LIGHT:																//Step 1 switch the first light on
				gHMISensorColor.sXPColorLamp01 := "LightButtonRedStyle";											//turn on Red in Visu
				gHMISensorColor.uXPColorLamp01 := 1;
				gXPIOModule.DOut.DOledRed01 := TRUE;										//Turn on Red on XPLamp
				IF TON_SwitchLamp01.Q AND gHMIsigSim.XPDIswitch[1] THEN						//if timer is counted and user hit button can change phase	 						
					Lamp01Step := SWITCH_SECOND_LIGHT;										//Change phase
					TON_SwitchLamp01(IN := FALSE, PT := 10);								//Reset timer
				END_IF;	
				
			SWITCH_SECOND_LIGHT:															//Step 2 switch the second light on
				gHMISensorColor.sXPColorLamp01 := "LightButtonYellowStyle";											//turn on Yellow in Visu
				gHMISensorColor.uXPColorLamp01 := 2;
				gXPIOModule.DOut.DOledYellow01 := TRUE;										//Turn on Yellow on XPLamp
				gXPIOModule.DOut.DOledRed01 := FALSE; 										//Switch off Output
				IF TON_SwitchLamp01.Q AND gHMIsigSim.XPDIswitch[1] THEN						//if timer is counted and user hit button can change phase							
					Lamp01Step := SWITCH_THIRD_LIGHT;										//Change phase
					TON_SwitchLamp01(IN := FALSE, PT := 10);								//Reset timer 
				END_IF;
				
			SWITCH_THIRD_LIGHT:																//Step 3 switch the third light on
				gHMISensorColor.sXPColorLamp01 := "LightButtonGreenStyle";											//turn on Green in Visu
				gHMISensorColor.uXPColorLamp01 := 3;
				gXPIOModule.DOut.DOledGreen01 := TRUE;										//Turn on Green on XPLamp
				gXPIOModule.DOut.DOledYellow01 := FALSE; 									//Switch off Output
				IF TON_SwitchLamp01.Q AND gHMIsigSim.XPDIswitch[1] THEN						//if timer is counted and user hit button can change phase
					Lamp01Step := SWITCH_OFF;												//Change phase
					TON_SwitchLamp01(IN := FALSE, PT := 10);								//Reset timer 
				END_IF;
			
			SWITCH_OFF:																		//Step 4 switch off the light
				gHMISensorColor.sXPColorLamp01 := "LightButtonGreyStyle";											//turn off Red in Visu
				gHMISensorColor.uXPColorLamp01 := 0;
				gXPIOModule.DOut.DOledGreen01 := FALSE;										//Turn on Green on XPLamp
				IF TON_SwitchLamp01.Q AND gHMIsigSim.XPDIswitch[1]  THEN					//if timer is counted and user hit button can change phase
					Lamp01Step := SWITCH_FIRST_LIGHT;										//change phase
					TON_SwitchLamp01(IN := FALSE, PT := 10);								//Reset timer	
				END_IF;
		END_CASE;

		TON_SwitchLamp02(IN := TRUE, PT := 10);												//TON for second XP button
		
		CASE Lamp02Step OF																	//Lamp 2 step case

			SWITCH_FIRST_LIGHT:																//Step 1 switch the first light on
				gHMISensorColor.sXPColorLamp02 := "LightButtonRedStyle";											//turn on Red in Visu
				gHMISensorColor.uXPColorLamp02 := 1;
				gXPIOModule.DOut.DOledRed02 := TRUE;										//Turn on Red on XPLamp
				IF TON_SwitchLamp02.Q AND gHMIsigSim.XPDIswitch[2] THEN						//if timer is counted and user hit button can change phase							
					Lamp02Step := SWITCH_SECOND_LIGHT;										//Change phase
					TON_SwitchLamp02(IN := FALSE, PT := 10);								//Reset timer
				END_IF;	
				
			SWITCH_SECOND_LIGHT:															//Step 2 switch the second light on
				gHMISensorColor.sXPColorLamp02 := "LightButtonYellowStyle";											//turn on Yellow in Visu
				gHMISensorColor.uXPColorLamp02 := 2;
				gXPIOModule.DOut.DOledYellow02 := TRUE;										//Turn on Yellow on XPLamp
				gXPIOModule.DOut.DOledRed02 := FALSE; 										//Switch off Output
				IF TON_SwitchLamp02.Q AND gHMIsigSim.XPDIswitch[2] THEN						//if timer is counted and user hit button can change phase							
					Lamp02Step := SWITCH_THIRD_LIGHT;										//Change phase
					TON_SwitchLamp02(IN := FALSE, PT := 10);								//Reset timer 
				END_IF;
				
			SWITCH_THIRD_LIGHT:																//Step 3 switch the third light on
				gHMISensorColor.sXPColorLamp02 := "LightButtonGreenStyle";											//turn on Green in Visu
				gHMISensorColor.uXPColorLamp02 := 3;
				gXPIOModule.DOut.DOledGreen02 := TRUE;										//Turn on Green on XPLamp
				gXPIOModule.DOut.DOledYellow02 := FALSE; 									//Switch off Output
				IF TON_SwitchLamp02.Q AND gHMIsigSim.XPDIswitch[2] THEN						//if timer is counted and user hit button can change phase
					Lamp02Step := SWITCH_OFF;												//Change phase
					TON_SwitchLamp02(IN := FALSE, PT := 10);								//Reset timer 
				END_IF;
			
			SWITCH_OFF:																		//Step 4 switch off the light
				gHMISensorColor.sXPColorLamp02 := "LightButtonGreyStyle";											//turn off Red in Visu
				gHMISensorColor.uXPColorLamp02 := 0;
				gXPIOModule.DOut.DOledGreen02 := FALSE;										//Turn on Green on XPLamp
				IF TON_SwitchLamp02.Q AND gHMIsigSim.XPDIswitch[2]  THEN					//if timer is counted and user hit button can change phase
					Lamp02Step := SWITCH_FIRST_LIGHT;										//Change phase
					TON_SwitchLamp02(IN := FALSE, PT := 10);								//Reset timer	
				END_IF;
		END_CASE;
		
		TON_SwitchLamp03(IN := TRUE, PT := 10);												//TON for third XP button
		
		CASE Lamp03Step OF																	//Lamp 3 step case
			
			SWITCH_FIRST_LIGHT:																//Step 1 switch the first light on
				gHMISensorColor.sXPColorLamp03 := "LightButtonRedStyle";											//turn on Red in Visu
				gHMISensorColor.uXPColorLamp03 := 1;
				gXPIOModule.DOut.DOledRed03 := TRUE;										//Turn on Red on XPLamp
				IF TON_SwitchLamp03.Q AND gHMIsigSim.XPDIswitch[3] THEN						//if timer is counted and user hit button can change phase							
					Lamp03Step := SWITCH_SECOND_LIGHT;										//Change phase
					TON_SwitchLamp03(IN := FALSE, PT := 10);								//Reset timer
				END_IF;	
				
			SWITCH_SECOND_LIGHT:															//Step 2 switch the second light on
				gHMISensorColor.sXPColorLamp03 := "LightButtonYellowStyle";											//turn on Yellow in Visu
				gHMISensorColor.uXPColorLamp03 := 2;
				gXPIOModule.DOut.DOledYellow03 := TRUE;										//Turn on Yellow on XPLamp
				gXPIOModule.DOut.DOledRed03 := FALSE; 										//Switch off Output
				IF TON_SwitchLamp03.Q AND gHMIsigSim.XPDIswitch[3] THEN						//if timer is counted and user hit button can change phase							
					Lamp03Step := SWITCH_THIRD_LIGHT;										//Change phase
					TON_SwitchLamp03(IN := FALSE, PT := 10);								//Reset timer 
				END_IF;
				
			SWITCH_THIRD_LIGHT:																//Step 3 switch the third light on
				gHMISensorColor.sXPColorLamp03 := "LightButtonGreenStyle";											//turn on Green in Visu
				gHMISensorColor.uXPColorLamp03 := 3;
				gXPIOModule.DOut.DOledGreen03 := TRUE;										//Turn on Green on XPLamp
				gXPIOModule.DOut.DOledYellow03 := FALSE; 									//Switch off Output
				IF TON_SwitchLamp03.Q AND gHMIsigSim.XPDIswitch[3] THEN						//if timer is counted and user hit button can change phase
					Lamp03Step := SWITCH_OFF;												//Change phase
					TON_SwitchLamp03(IN := FALSE, PT := 10);								//Reset timer 
				END_IF;
			
			SWITCH_OFF:																		//Step 4 switch off the light
				gHMISensorColor.sXPColorLamp03 := "LightButtonGreyStyle";											//turn off Red in Visu
				gHMISensorColor.uXPColorLamp03 := 0;
				gXPIOModule.DOut.DOledGreen03 := FALSE;										//Turn on Green on XPLamp
				IF TON_SwitchLamp03.Q AND gHMIsigSim.XPDIswitch[3]  THEN					//if timer is counted and user hit button can change phase
					Lamp03Step := SWITCH_FIRST_LIGHT;										//Change phase
					TON_SwitchLamp03(IN := FALSE, PT := 10);								//Reset timer
				END_IF;
		END_CASE;								
		
		TON_SwitchLamp04(IN := TRUE, PT := 10);												//TON for fourth XP button
		
		CASE Lamp04Step OF																	//Lamp 1 step case
			
			SWITCH_FIRST_LIGHT:																//Step 1 switch the first light on
				gHMISensorColor.sXPColorLamp04 := "LightButtonRedStyle";											//turn on Red in Visu
				gHMISensorColor.uXPColorLamp04 := 1;
				gXPIOModule.DOut.DOledRed04 := TRUE;										//Turn on Red on XPLamp
				IF TON_SwitchLamp04.Q AND gHMIsigSim.XPDIswitch[4] THEN						//if timer is counted and user hit button can change phase							
					Lamp04Step := SWITCH_SECOND_LIGHT;										//Change phase
					TON_SwitchLamp04(IN := FALSE, PT := 10);								//Reset timer
				END_IF;	
				
			SWITCH_SECOND_LIGHT:															//Step 2 switch the second light on
				gHMISensorColor.sXPColorLamp04 := "LightButtonYellowStyle";											//turn on Yellow in Visu
				gHMISensorColor.uXPColorLamp04 := 2;
				gXPIOModule.DOut.DOledYellow04 := TRUE;										//Turn on Yellow on XPLamp
				gXPIOModule.DOut.DOledRed04 := FALSE; 										//Switch off Output
				IF TON_SwitchLamp04.Q AND gHMIsigSim.XPDIswitch[4] THEN						//if timer is counted and user hit button can change phase							
					Lamp04Step := SWITCH_THIRD_LIGHT;										//Change phase
					TON_SwitchLamp04(IN := FALSE, PT := 10);								//Reset timer 
				END_IF;
				
			SWITCH_THIRD_LIGHT:																//Step 3 switch the third light on
				gHMISensorColor.sXPColorLamp04 := "LightButtonGreenStyle";											//turn on Green in Visu
				gHMISensorColor.uXPColorLamp04 := 3;
				gXPIOModule.DOut.DOledGreen04 := TRUE;										//Turn on Green on XPLamp
				gXPIOModule.DOut.DOledYellow04 := FALSE; 									//Switch off Output
				IF TON_SwitchLamp04.Q AND gHMIsigSim.XPDIswitch[4] THEN						//if timer is counted and user hit button can change phase
					Lamp04Step := SWITCH_OFF;												//Change phase
					TON_SwitchLamp04(IN := FALSE, PT := 10);								//Reset timer 
				END_IF;
			
			SWITCH_OFF:																		//Step 4 switch off the light
				gHMISensorColor.sXPColorLamp04 := "LightButtonGreyStyle";											//turn off Red in Visu
				gHMISensorColor.uXPColorLamp04 := 0;
				gXPIOModule.DOut.DOledGreen04 := FALSE;										//Turn on Green on XPLamp
				IF TON_SwitchLamp04.Q AND gHMIsigSim.XPDIswitch[4]  THEN					//if timer is counted and user hit button can change phase
					Lamp04Step := SWITCH_FIRST_LIGHT;										//Change phase
					TON_SwitchLamp04(IN := FALSE, PT := 10);								//Reset timer
				END_IF;
		END_CASE;
			
		IF anlog_out_tmp01<>0 THEN															//Convert the Volt signal from HMI panel to signal for analog output
			gHMIsigSim.EXTAnalogOut01 := anlog_out_tmp01;
			anlog_out_tmp01:=0;
		END_IF
		
		IF anlog_out_tmp02<>0 THEN															//Convert the Volt signal from HMI panel to signal for analog output
			
			gHMIsigSim.EXTAnalogOut02 := anlog_out_tmp02;
			anlog_out_tmp02:=0;
		END_IF
		
		gEXTIOModule.AOut.AnalogDisplay01 := REAL_TO_INT(gHMIsigSim.EXTAnalogOut01 / INT_TO_V);
		gEXTIOModule.AOut.AnalogDisplay02 := REAL_TO_INT(gHMIsigSim.EXTAnalogOut02 / INT_TO_V);
		
		gEXTIOModule.AInp.trimmer01_deg := (SimPlantAI1 * V_TO_DEG);
		gEXTIOModule.AInp.trimmer02_deg := (SimPlantAI2 * V_TO_DEG);
		
		brsftoa(gEXTIOModule.AInp.trimmer01_deg,ADR(TrimmerDeg));
		brsstrcpy(ADR(gTransformWordTrimmer1),ADR('[{"select":"#pointerKnob1","spin":['));
		brsstrcat(ADR(gTransformWordTrimmer1),ADR(TrimmerDeg));
		brsstrcat(ADR(gTransformWordTrimmer1),ADR(',0,0],"display":true}]'));
		
		brsftoa(gEXTIOModule.AInp.trimmer02_deg,ADR(TrimmerDeg));
		brsstrcpy(ADR(gTransformWordTrimmer2),ADR('[{"select":"#pointerKnob2","spin":['));
		brsstrcat(ADR(gTransformWordTrimmer2),ADR(TrimmerDeg));
		brsstrcat(ADR(gTransformWordTrimmer2),ADR(',0,0],"display":true}]'));
		
		// *** new only for 1 client !!!***
		IF Infotainment.Manual.isOpenFlyOut[0] THEN
			//Infotainment.Automatic.Dialog.video_pdfFileNum_temp[0] := Infotainment.Automatic.Dialog.video_pdfFileNum[0];
			Infotainment.Automatic.Dialog.video_pdfFileNum[0] := Infotainment.Automatic.Banner.actualNum[0];
			Infotainment.Automatic.Dialog.videoWord[0] := VIDEO_INFOTAINMENT_LIST[Infotainment.Automatic.Dialog.video_pdfFileNum[0]];
			Infotainment.Automatic.Dialog.pdfWord[0] := PDF_INFOTAINMENT_LIST[Infotainment.Automatic.Dialog.video_pdfFileNum[0]];
			Infotainment.Manual.waitForCloseFlyOut[0] := TRUE;
		END_IF;
		// ***
	END_IF;
	
END_PROGRAM
