(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * PROGRAM: modSwitch
 * File: modSwitch.typ
 * Author: Bubenik
 * Created: February 26, 2016
 ********************************************************************
 * Implementation OF PROGRAM modSwitch
 ********************************************************************)

TYPE
	caseMode_enum : 
		( (*Main enum for switch*)
		PANEL,
		DEMO,
		SIMUL,
		EMPTY
		);
END_TYPE
