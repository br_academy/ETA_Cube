﻿<?xml version="1.0" encoding="utf-8"?>
<?AutomationStudio Version=4.2.3.159?>
<Program xmlns="http://br-automation.co.at/AS/Program">
  <Files>
    <File Description="Cyclic switch mod">modSwitchCyclic.st</File>
    <File Description="Init switch mod">modSwitchInit.st</File>
    <File Description="Local data types" Private="true">modSwitch.typ</File>
    <File Description="Local variables" Private="true">modSwitch.var</File>
  </Files>
</Program>