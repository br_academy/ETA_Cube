(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * PROGRAM: modSwitch
 * File: modSwitchInit.st
 * Author: Bubenik
 * Created: February 26, 2016
 ********************************************************************
 * Implementation OF PROGRAM modSwitch
 ********************************************************************)

PROGRAM _INIT
	
	IF IdleTime = 0 THEN										//Init time for switch to Demo mode
		IdleTime := 500;
	END_IF;
	
	IF trimmer01_sens = 0 THEN									//Init sensitivity for potentiomer
		trimmer01_sens := 200;
	END_IF;
	
	IF trimmer02_sens = 0 THEN									//Init sensitivity for potentiomer
		trimmer02_sens := 200;
	END_IF;	
	
	hours := IdleTime / 360000;									//Calculated hours from Idle Time
	minutes := (IdleTime-(360000*hours))/6000;					//Calculated minutes from Idle Time
	seconds := ((IdleTime-((6000*minutes)+(hours*360000)))/100);//Calculated seconds from Idle Time
	
	PanelEnabled :=TRUE;										//All modes enable
	SimulEnabled := TRUE;
	DemoEnabled := TRUE;
		
	SimPlantEncOffset := 0;										//Offset of encoder
	
END_PROGRAM