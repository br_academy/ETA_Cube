(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * PROGRAM: modSwitch
 * File: modSwitchCyclic.st
 * Author: Bubenik
 * Created: February 26, 2016
 ********************************************************************
 * Implementation OF PROGRAM modSwitch
 ********************************************************************)

PROGRAM _CYCLIC

	FOR i:=0 TO (gCLIENTS_COUNT) DO														//I find how many valid users are connecting to mapp View(I want to work only with number of valid users, not with all users)
		IF (gVisu.ClientInfo[i].isValid) THEN
			gVisu.userValidCount := gVisu.userValidCount + 1;
		END_IF;
	END_FOR;
	
	IF (IdleTime <>((hours*360000)+(minutes*6000)+(seconds*100))) THEN					//Condition when user change idle time in Visu
		IdleTime := ((hours*360000)+(minutes*6000)+(seconds*100));
		caseMode:=PANEL;																//if user change time mode change to panel
		TON_IdleTime(IN := FALSE);														//turn of timer
	END_IF;
	
	IF (caseMode=PANEL OR caseMode=SIMUL) THEN											//Panel mode or simulation enables timer for demo
		TON_IdleTime(IN := TRUE, PT := IdleTime);
	ELSE TON_IdleTime(IN := FALSE);														//Reset timer in case of Demo mode	
	END_IF;
	
	FOR i:=1 TO 8 DO																	//Check all software switches in For loop
		RF_TRIG_SIMUL[i]( CLK:=gHMIsigSim.X20DIswitch[i] );								//check both edges of software switches			
		IF RF_TRIG_SIMUL[i].Q THEN														//If find same change reset Idle timer
			IF caseMode<>SIMUL THEN														
				SoftButTouch := i;														//if mode is not SIMUL then save index of touched button for immediately turn on
			END_IF;
			TON_IdleTime(IN := FALSE);													//reset Idle timer
			caseMode:=SIMUL; 															//Turn on Simulation mode
			EXIT;																		//Exit from loop
		END_IF;
	END_FOR;
	
	FOR i:=1 TO 4 DO																	
		IF gHMIsigSim.XPDIswitch[i]	THEN												//check rising edge of color software buttons
			caseMode:=SIMUL;															//Turn to simulation mode
			TON_IdleTime(IN := FALSE);													//reset idle timer
			EXIT;																		//Exit from loop
		END_IF;
	END_FOR;
	
	RF_TRIG_COOLING (CLK:=gEXTIOModule.DOut.Cooling);									//check both edges of Cooling switch 
	RF_TRIG_HEATING (CLK:=gEXTIOModule.DOut.Heating);									//check both edges of Heating switch 
	IF RF_TRIG_COOLING.Q OR RF_TRIG_HEATING.Q THEN										//Check if pressed cooling or heating
		caseMode:=SIMUL;																//Turn to simulation mode
		TON_IdleTime(IN := FALSE);														//reset idle timer
	END_IF;
	
	//new***
	IF setModeSimul THEN
		caseMode:=SIMUL;
		setModeSimul := FALSE;
	END_IF;
	//***
		
	F_TRIG_RFID (CLK := INT_TO_BOOL(Allow_rights));										//recognize  falling edge that determines connected and validated RFID Chip
	IF F_TRIG_RFID.Q THEN																//if validation is OK 
		caseMode := PANEL;																//Turn to panel mode
		TON_IdleTime(IN := FALSE);														//reset idle timer
	END_IF;
	
	FOR i:=1 TO 8 DO																	//Check all Hardware switches in For loop
		RF_TRIG_PANEL[i]( CLK:=gX20IOModule.DInp.DIswitch[i] );							//check both edges of hardware switches
		IF RF_TRIG_PANEL[i].Q THEN														//If find same change reset Idle timer
			caseMode:=PANEL;															//Turn on Panel mode
			TON_IdleTime(IN := FALSE);													//Reset idle timer
			EXIT;																		//Exit from loop
		END_IF;
	END_FOR;

	FOR i:=1 TO 4 DO																	
		IF gXPIOModule.DInp.DIswitch[i] THEN											//check rising edge of color hardware buttons
			caseMode:=PANEL;															//Turn on Panel mode
			TON_IdleTime(IN := FALSE);													//Reset idle timer
			EXIT;																		//Exit from loop
		END_IF;
	END_FOR;

	//checking change of value in range of +-trimmmer_sens
	IF (((trimmer01_tmp+trimmer01_sens) < gEXTIOModule.AInp.trimmer01) OR ((trimmer01_tmp-trimmer01_sens) > gEXTIOModule.AInp.trimmer01)) THEN
		caseMode:= PANEL;
		trimmer01_tmp:=gEXTIOModule.AInp.trimmer01;
		TON_IdleTime(IN := FALSE);
	END_IF;
	
	//checking change of value in range of +-trimmmer_sens
	IF (((trimmer02_tmp+trimmer02_sens) < gEXTIOModule.AInp.trimmer02) OR ((trimmer02_tmp-trimmer02_sens) > gEXTIOModule.AInp.trimmer02)) THEN
		caseMode:= PANEL;
		trimmer02_tmp:=gEXTIOModule.AInp.trimmer02;
		TON_IdleTime(IN := FALSE);
	END_IF;
	
	IF 	(Encoder01Tmp <> gEXTIOModule.Encorder.Encoder01) THEN						//check changes of Encoder
		Encoder01Tmp:=gEXTIOModule.Encorder.Encoder01;
		caseMode:= PANEL;
		TON_IdleTime(IN := FALSE);
	END_IF;
	
	//** added AND (NOT Infotainment.Manual.isOpenFlyOut[i])
	IF (TON_IdleTime.Q AND ((caseMode = PANEL) OR (caseMode = SIMUL)) AND (NOT Infotainment.Manual.isOpenFlyOut[0])) THEN			//condition to switch to demo mode
		caseMode:=DEMO;																//Turn on Demo mode
		InitDemo :=TRUE;															//Turn on Init in Panel mode
		TON_IdleTime(IN := FALSE);													//Turn off Idle timer
	END_IF;	
	
	CASE caseMode OF																//Main Mode switch
		DEMO: 																		//DEMO
			IF DemoEnabled THEN														
				demoOut1:= TRUE;													//Demo mod turn on
				panelOut1:=FALSE;													//Panel mod turn off	
				simulOut1:=FALSE;													//Simulation mod turn off
				InitPanel := TRUE;													//Init of Panel turn on
				InitSimul := TRUE;													//Init of Simulation turn on
			ELSE 
				caseMode := EMPTY;													//condition when user didnt choose any mode
				InitDemo := TRUE;													//Init of Simulation turn on
			END_IF;
		
		PANEL: 																		//PANEL
			IF PanelEnabled THEN
				demoOut1:= FALSE;													//Demo mod turn off
				panelOut1:=TRUE;													//Panel mod turn on
				simulOut1:=FALSE;													//Simul mod turn off
				InitSimul:= TRUE;													//Init of Simulation turn on
				InitDemo := TRUE;													//Init of Demo turn on
			ELSE 
				caseMode := EMPTY;													//condition when user didnt choose any mode
				InitPanel := TRUE;													//Init of Panel turn on
			END_IF;
		
		SIMUL: 																		//SIMUL
			IF SimulEnabled THEN													
				demoOut1:= FALSE;													//Demo mod turn off
				panelOut1:=FALSE;													//Panel mod turn off												
				simulOut1:=TRUE;													//Simulation mod turn on
				InitDemo := TRUE;													//Init of Demo turn on
				InitPanel := TRUE;													//Init of Panel turn on

			ELSE 
				caseMode := EMPTY;													//condition when user didnt choose any mode
				InitSimul := TRUE;													//Init of Simulation turn on
			END_IF;
	
		EMPTY: 																		//EMPTY - case when user is in for example in panel mode and he turn off it in settings
			IF PanelEnabled AND NOT SimulEnabled THEN								
				caseMode := PANEL;													
			ELSIF NOT PanelEnabled AND SimulEnabled THEN
				caseMode := SIMUL;													
			ELSIF PanelEnabled AND SimulEnabled THEN
				caseMode := PANEL;													//Panel has a higher priority
			ELSIF (NOT PanelEnabled) AND (NOT SimulEnabled) AND (DemoEnabled) THEN
				caseMode := DEMO;
			ELSE
				demoOut1:= FALSE;													//all modes are turn off
				panelOut1:=FALSE;
				simulOut1:=FALSE;
				caseMode:=EMPTY;
			END_IF;

	END_CASE; 
	
	SimuPlatTemp := (gEXTIOModule.ATemp.SimTemp / 10.0);							//Convert the Temperature value
	
	IF gEXTIOModule.Encorder.DigitalInput01 <> DigitalInput01tmp THEN				//When Clicked on Encoder set up of new zero
		DigitalInput01tmp := gEXTIOModule.Encorder.DigitalInput01;
		SimPlantEncOffset := SimPlantEnc;
	END_IF;
	
	IF BOOL_TO_DINT(gEXTIOModule.Encorder.DigitalInput02) <> DigitalInput02tmp THEN				//When Clicked on Encoder set up of new zero
		DigitalInput02tmp := gEXTIOModule.Encorder.DigitalInput02;
		SimPlantEncOffset := SimPlantEnc;
	END_IF;
	
	SimPlantEnc := DINT_TO_REAL((gEXTIOModule.Encorder.Encoder01+gEXTIOModule.Encorder.Encoder02) * 6);	//Real degrees
	EncRotation:= SimPlantEnc  - SimPlantEncOffset;									//Offset after click on Encoder
			
	IF (EncRotation >= 360 ) THEN
		IF (EncRotationVisu >= 360 ) THEN
			NumOfRev := NumOfRev +1;
		END_IF;	
		EncRotationVisu := EncRotation - 360*ABS(NumOfRev);	
	ELSIF (EncRotation <= -360) THEN
		IF (EncRotationVisu <= -360 ) THEN
			NumOfRev := NumOfRev -1;
		END_IF;																		//If the Degree are more then 1 rotation back	
		EncRotationVisu := EncRotation + 360*ABS(NumOfRev);	
	ELSE
		EncRotationVisu := EncRotation;
		NumOfRev := 0;
	END_IF
	
	brsftoa(EncRotationVisu,ADR(EncoderDeg));
	brsstrcpy(ADR(gTransformWordEncoder),ADR('[{"select":"#pointerEnc","spin":['));
	brsstrcat(ADR(gTransformWordEncoder),ADR(EncoderDeg));
	brsstrcat(ADR(gTransformWordEncoder),ADR(',0,0],"display":true}]')); 			
	
	IF EncRotation<0 THEN															//setting for Encoder Visu to +-360 degrees simulation
		SimPlantEncVisuMin := -360;													//floating value for -360
		SimPlantEncVisuMax := 0;
	ELSE 
		SimPlantEncVisuMin := 0;													//floating value for +360
		SimPlantEncVisuMax := 360;
	END_IF;
	
END_PROGRAM
