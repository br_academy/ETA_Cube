(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * PROGRAM: modPanel
 * File: modPanel.st
 * Author: Bubenik
 * Created: February 26, 2016
 ********************************************************************
 * Implementation OF PROGRAM modPanel
 ********************************************************************)

PROGRAM _CYCLIC
	
	IF panelOut1 THEN																//enables control by panel
		
		 
		IF (InitPanel) THEN															//Initial conditions
			gHMISensorColor.sXPColorLamp01 := "LightButtonGreyStyle";
			gHMISensorColor.sXPColorLamp02 := "LightButtonGreyStyle";
			gHMISensorColor.sXPColorLamp03 := "LightButtonGreyStyle";
			gHMISensorColor.sXPColorLamp04 := "LightButtonGreyStyle";
			gHMISensorColor.uXPColorLamp01 := 0;
			gHMISensorColor.uXPColorLamp02 := 0;
			gHMISensorColor.uXPColorLamp03 := 0;
			gHMISensorColor.uXPColorLamp04 := 0;
			Switch01Pressed := FALSE;
			Switch02Pressed := FALSE;
			Switch03Pressed := FALSE;
			Switch04Pressed := FALSE;
			gXPIOModule.DOut.DOledRed01 := FALSE;
			gXPIOModule.DOut.DOledRed02 := FALSE;
			gXPIOModule.DOut.DOledRed03 := FALSE;
			gXPIOModule.DOut.DOledRed04 := FALSE;
			gXPIOModule.DOut.DOledYellow01 := FALSE;
			gXPIOModule.DOut.DOledYellow02 := FALSE;
			gXPIOModule.DOut.DOledYellow03 := FALSE;
			gXPIOModule.DOut.DOledYellow04 := FALSE;
			gXPIOModule.DOut.DOledGreen01 := FALSE;
			gXPIOModule.DOut.DOledGreen02 := FALSE;
			gXPIOModule.DOut.DOledGreen03 := FALSE;
			gXPIOModule.DOut.DOledGreen04 := FALSE;
			Lamp01Step := SWITCH_OFF;
			Lamp02Step := SWITCH_OFF;
			Lamp03Step := SWITCH_OFF;
			Lamp04Step := SWITCH_OFF;
			InitPanel:=FALSE;
		END_IF;
		
		FOR i:=1 TO 8 DO
			gX20IOModule.DOut.DOled[i]:= gX20IOModule.DInp.DIswitch[i];							//get back DI settings before mode change	
			gHMISensorColor.X20DIswitch[i] := BOOL_TO_USINT(gX20IOModule.DInp.DIswitch[i]);		//change to red colour every forced button in visu

		END_FOR;

		TON_SwitchLamp01(IN := TRUE, PT := 10);													//TON for first XP button
		
		CASE Lamp01Step OF																		//Lamp 1 step case
			
			SWITCH_FIRST_LIGHT:																	//Step 1 switch the first light on
				gHMISensorColor.sXPColorLamp01 := "LightButtonRedStyle";												//turn on Red in Visu
				gHMISensorColor.uXPColorLamp01 := 1;
				gXPIOModule.DOut.DOledRed01 := TRUE;											//Turn on Red on XPLamp
				IF TON_SwitchLamp01.Q AND gXPIOModule.DInp.DIswitch[1] THEN						//if timer is counted and user hit button can change phase							
					Lamp01Step := SWITCH_SECOND_LIGHT;											//Change phase
					TON_SwitchLamp01(IN := FALSE, PT := 10);									//Reset timer
				END_IF;	
				
			SWITCH_SECOND_LIGHT:																//Step 2 switch the second light on
				gHMISensorColor.sXPColorLamp01 := "LightButtonYellowStyle";												//turn on Yellow in Visu
				gHMISensorColor.uXPColorLamp01 := 2;
				gXPIOModule.DOut.DOledYellow01 := TRUE;											//Turn on Yellow on XPLamp
				gXPIOModule.DOut.DOledRed01 := FALSE; 											//Switch off Output
				IF TON_SwitchLamp01.Q AND gXPIOModule.DInp.DIswitch[1] THEN						//if timer is counted and user hit button can change phase							
					Lamp01Step := SWITCH_THIRD_LIGHT;											//Change phase
					TON_SwitchLamp01(IN := FALSE, PT := 10);									//Reset timer 
				END_IF;
				
			SWITCH_THIRD_LIGHT:																	//Step 3 switch the third light on
				gHMISensorColor.sXPColorLamp01 := "LightButtonGreenStyle";												//turn on Green in Visu
				gHMISensorColor.uXPColorLamp01 := 3;
				gXPIOModule.DOut.DOledGreen01 := TRUE;											//Turn on Green on XPLamp
				gXPIOModule.DOut.DOledYellow01 := FALSE; 										//Switch off Output
				IF TON_SwitchLamp01.Q AND gXPIOModule.DInp.DIswitch[1] THEN						//if timer is counted and user hit button can change phase
					Lamp01Step := SWITCH_OFF;													//Change phase
					TON_SwitchLamp01(IN := FALSE, PT := 10);									//Reset timer 
				END_IF;
			
			SWITCH_OFF:																			//Step 4 switch off the light
				gHMISensorColor.sXPColorLamp01 := "LightButtonGreyStyle";												//turn off Red in Visu
				gHMISensorColor.uXPColorLamp01 := 0;
				gXPIOModule.DOut.DOledGreen01 := FALSE;											//Turn on Green on XPLamp
				IF TON_SwitchLamp01.Q AND gXPIOModule.DInp.DIswitch[1]  THEN					//if timer is counted and user hit button can change phase
					Lamp01Step := SWITCH_FIRST_LIGHT;											//Change phase
					TON_SwitchLamp01(IN := FALSE, PT := 10);									//Reset timer 
				END_IF;
		END_CASE;

		TON_SwitchLamp02(IN := TRUE, PT := 10);													//TON for second XP button
		
		CASE Lamp02Step OF																		//Lamp 2 step case
			
			SWITCH_FIRST_LIGHT:																	//Step 1 switch the first light on
				gHMISensorColor.sXPColorLamp02 := "LightButtonRedStyle";												//turn on Red in Visu
				gHMISensorColor.uXPColorLamp02 := 1;
				gXPIOModule.DOut.DOledRed02 := TRUE;											//Turn on Red on XPLamp
				IF TON_SwitchLamp02.Q AND gXPIOModule.DInp.DIswitch[2] THEN						//if timer is counted and user hit button can change phase							
					Lamp02Step := SWITCH_SECOND_LIGHT;											//Change phase
					TON_SwitchLamp02(IN := FALSE, PT := 10);									//Reset timer
				END_IF;	
				
			SWITCH_SECOND_LIGHT:																//Step 2 switch the second light on
				gHMISensorColor.sXPColorLamp02 := "LightButtonYellowStyle";												//turn on Yellow in Visu
				gHMISensorColor.uXPColorLamp02 := 2;
				gXPIOModule.DOut.DOledYellow02 := TRUE;											//Turn on Yellow on XPLamp
				gXPIOModule.DOut.DOledRed02 := FALSE; 											//Switch off Output
				IF TON_SwitchLamp02.Q AND gXPIOModule.DInp.DIswitch[2] THEN						//if timer is counted and user hit button can change phase							
					Lamp02Step := SWITCH_THIRD_LIGHT;											//Change phase
					TON_SwitchLamp02(IN := FALSE, PT := 10);									//Reset timer 
				END_IF;
				
			SWITCH_THIRD_LIGHT:																	//Step 3 switch the third light on
				gHMISensorColor.sXPColorLamp02 := "LightButtonGreenStyle";												//turn on Green in Visu
				gHMISensorColor.uXPColorLamp02 := 3;
				gXPIOModule.DOut.DOledGreen02 := TRUE;											//Turn on Green on XPLamp
				gXPIOModule.DOut.DOledYellow02 := FALSE; 										//Switch off Output
				IF TON_SwitchLamp02.Q AND gXPIOModule.DInp.DIswitch[2] THEN						//if timer is counted and user hit button can change phase
					Lamp02Step := SWITCH_OFF;													//Change phase
					TON_SwitchLamp02(IN := FALSE, PT := 10);									//Reset timer 
				END_IF;
			
			SWITCH_OFF:																			//Step 4 switch off the light
				gHMISensorColor.sXPColorLamp02 := "LightButtonGreyStyle";												//turn off Red in Visu
				gHMISensorColor.uXPColorLamp02 := 0;
				gXPIOModule.DOut.DOledGreen02 := FALSE;											//Turn on Green on XPLamp
				IF TON_SwitchLamp02.Q AND gXPIOModule.DInp.DIswitch[2]  THEN					//if timer is counted and user hit button can change phase
					Lamp02Step := SWITCH_FIRST_LIGHT;											//Change phase
					TON_SwitchLamp02(IN := FALSE, PT := 10);									//Reset timer 
				END_IF;
		END_CASE;
		
		TON_SwitchLamp03(IN := TRUE, PT := 10);													//TON for third XP button
		
		CASE Lamp03Step OF																		//Lamp 1 step case
			
			SWITCH_FIRST_LIGHT:																	//Step 1 switch the first light on
				gHMISensorColor.sXPColorLamp03 := "LightButtonRedStyle";												//turn on Red in Visu
				gHMISensorColor.uXPColorLamp03 := 1;
				gXPIOModule.DOut.DOledRed03 := TRUE;											//Turn on Red on XPLamp
				IF TON_SwitchLamp03.Q AND gXPIOModule.DInp.DIswitch[3] THEN						//if timer is counted and user hit button can change phase							
					Lamp03Step := SWITCH_SECOND_LIGHT;											//Change phase
					TON_SwitchLamp03(IN := FALSE, PT := 10);									//Reset timer
				END_IF;	
				
			SWITCH_SECOND_LIGHT:																//Step 2 switch the second light on
				gHMISensorColor.sXPColorLamp03 := "LightButtonYellowStyle";												//turn on Yellow in Visu
				gHMISensorColor.uXPColorLamp03 := 2;
				gXPIOModule.DOut.DOledYellow03 := TRUE;											//Turn on Yellow on XPLamp
				gXPIOModule.DOut.DOledRed03 := FALSE; 											//Switch off Output
				IF TON_SwitchLamp03.Q AND gXPIOModule.DInp.DIswitch[3] THEN						//if timer is counted and user hit button can change phase							
					Lamp03Step := SWITCH_THIRD_LIGHT;											//Change phase
					TON_SwitchLamp03(IN := FALSE, PT := 10);									//Reset timer 
				END_IF;
				
			SWITCH_THIRD_LIGHT:																	//Step 3 switch the third light on
				gHMISensorColor.sXPColorLamp03 := "LightButtonGreenStyle";												//turn on Green in Visu
				gHMISensorColor.uXPColorLamp03 := 3;
				gXPIOModule.DOut.DOledGreen03 := TRUE;											//Turn on Green on XPLamp
				gXPIOModule.DOut.DOledYellow03 := FALSE; 										//Switch off Output
				IF TON_SwitchLamp03.Q AND gXPIOModule.DInp.DIswitch[3] THEN						//if timer is counted and user hit button can change phase
					Lamp03Step := SWITCH_OFF;													//Change phase
					TON_SwitchLamp03(IN := FALSE, PT := 10);									//Reset timer 
				END_IF;
			
			SWITCH_OFF:																			//Step 4 switch off the light
				gHMISensorColor.sXPColorLamp03 := "LightButtonGreyStyle";												//turn off Red in Visu
				gHMISensorColor.uXPColorLamp03 := 0;
				gXPIOModule.DOut.DOledGreen03 := FALSE;											//Turn on Green on XPLamp
				IF TON_SwitchLamp03.Q AND gXPIOModule.DInp.DIswitch[3]  THEN					//if timer is counted and user hit button can change phase
					Lamp03Step := SWITCH_FIRST_LIGHT;											//Change phase
					TON_SwitchLamp03(IN := FALSE, PT := 10);									//Reset timer 
				END_IF;
		END_CASE;
		
		TON_SwitchLamp04(IN := TRUE, PT := 10);													//TON for fourth XP button
		
		CASE Lamp04Step OF																		//Lamp 1 step case
			
			SWITCH_FIRST_LIGHT:																	//Step 1 switch the first light on
				gHMISensorColor.sXPColorLamp04 := "LightButtonRedStyle";												//turn on Red in Visu
				gHMISensorColor.uXPColorLamp04 := 1;
				gXPIOModule.DOut.DOledRed04 := TRUE;											//Turn on Red on XPLamp
				IF TON_SwitchLamp04.Q AND gXPIOModule.DInp.DIswitch[4] THEN						//if timer is counted and user hit button can change phase							
					Lamp04Step := SWITCH_SECOND_LIGHT;											//Change phase
					TON_SwitchLamp04(IN := FALSE, PT := 10);									//Reset timer
				END_IF;	
				
			SWITCH_SECOND_LIGHT:																//Step 2 switch the second light on
				gHMISensorColor.sXPColorLamp04 := "LightButtonYellowStyle";												//turn on Yellow in Visu
				gHMISensorColor.uXPColorLamp04 := 2;
				gXPIOModule.DOut.DOledYellow04 := TRUE;											//Turn on Yellow on XPLamp
				gXPIOModule.DOut.DOledRed04 := FALSE; 											//Switch off Output
				IF TON_SwitchLamp04.Q AND gXPIOModule.DInp.DIswitch[4] THEN						//if timer is counted and user hit button can change phase							
					Lamp04Step := SWITCH_THIRD_LIGHT;											//Change phase
					TON_SwitchLamp04(IN := FALSE, PT := 10);									//Reset timer 
				END_IF;
				
			SWITCH_THIRD_LIGHT:																	//Step 3 switch the third light on
				gHMISensorColor.sXPColorLamp04 := "LightButtonGreenStyle";												//turn on Green in Visu
				gHMISensorColor.uXPColorLamp04 := 3;
				gXPIOModule.DOut.DOledGreen04 := TRUE;											//Turn on Green on XPLamp
				gXPIOModule.DOut.DOledYellow04 := FALSE; 										//Switch off Output
				IF TON_SwitchLamp04.Q AND gXPIOModule.DInp.DIswitch[4] THEN						//if timer is counted and user hit button can change phase
					Lamp04Step := SWITCH_OFF;													//Change phase
					TON_SwitchLamp04(IN := FALSE, PT := 10);									//Reset timer 
				END_IF;
			
			SWITCH_OFF:																			//Step 4 switch off the light
				gHMISensorColor.sXPColorLamp04 := "LightButtonGreyStyle";												//turn off Red in Visu
				gHMISensorColor.uXPColorLamp04 := 0;
				gXPIOModule.DOut.DOledGreen04 := FALSE;											//Turn on Green on XPLamp
				IF TON_SwitchLamp04.Q AND gXPIOModule.DInp.DIswitch[4]  THEN					//if timer is counted and user hit button can change phase
					Lamp04Step := SWITCH_FIRST_LIGHT;											//Change phase
					TON_SwitchLamp04(IN := FALSE, PT := 10);									//Reset timer
				END_IF;
		END_CASE;
		
		
		IF gHMIsigSim.EXTAnalogOut01< anlog_out_tmp01-0.1 OR gHMIsigSim.EXTAnalogOut01> anlog_out_tmp01+0.1THEN	//Potentiomer P1 sensitivity set up
			anlog_out_tmp01 := gHMIsigSim.EXTAnalogOut01;
		END_IF;
		
		IF gHMIsigSim.EXTAnalogOut02< anlog_out_tmp02-0.1 OR gHMIsigSim.EXTAnalogOut02> anlog_out_tmp02+0.1THEN	//Potentiomer P2 sensitivity set up
			anlog_out_tmp02 := gHMIsigSim.EXTAnalogOut02;
		END_IF;
		
		gEXTIOModule.AInp.trimmer01_deg := INT_TO_REAL (gEXTIOModule.AInp.trimmer01) * INT_TO_DEG;								//Convert the Volt signal from HMI panel to signal for analog output
		gHMIsigSim.EXTAnalogOut01:=  gEXTIOModule.AInp.trimmer01_deg * DEG_TO_V;
		
		gEXTIOModule.AInp.trimmer02_deg := INT_TO_REAL (gEXTIOModule.AInp.trimmer02) * INT_TO_DEG;
		gHMIsigSim.EXTAnalogOut02:= gEXTIOModule.AInp.trimmer02_deg * DEG_TO_V;
		
		gEXTIOModule.AOut.AnalogDisplay01:= REAL_TO_INT (gEXTIOModule.AInp.trimmer01);
		gEXTIOModule.AOut.AnalogDisplay02:= REAL_TO_INT (gEXTIOModule.AInp.trimmer02);
		
		SimPlantAI1 := (gEXTIOModule.AInp.trimmer01_deg * DEG_TO_V);												//Convert analog value for visualization 0-10V
		SimPlantAI2 := (gEXTIOModule.AInp.trimmer02_deg * DEG_TO_V);
		
		brsftoa(gEXTIOModule.AInp.trimmer01_deg,ADR(TrimmerDeg));
		brsstrcpy(ADR(gTransformWordTrimmer1),ADR('[{"select":"#pointerKnob1","spin":['));
		brsstrcat(ADR(gTransformWordTrimmer1),ADR(TrimmerDeg));
		brsstrcat(ADR(gTransformWordTrimmer1),ADR(',0,0],"display":true}]'));
		
		brsftoa(gEXTIOModule.AInp.trimmer02_deg,ADR(TrimmerDeg));
		brsstrcpy(ADR(gTransformWordTrimmer2),ADR('[{"select":"#pointerKnob2","spin":['));
		brsstrcat(ADR(gTransformWordTrimmer2),ADR(TrimmerDeg));
		brsstrcat(ADR(gTransformWordTrimmer2),ADR(',0,0],"display":true}]'));
	
	END_IF;
END_PROGRAM