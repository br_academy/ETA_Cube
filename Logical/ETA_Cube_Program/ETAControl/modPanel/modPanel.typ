(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * PROGRAM: modPanel
 * File: modPanel.typ
 * Author: Bubenik
 * Created: February 26, 2016
 ********************************************************************
 * Implementation OF PROGRAM modPanel
 ********************************************************************)

TYPE
	LampStep_Enum : 
		( (*Lamp step case*)
		SWITCH_FIRST_LIGHT, (*Switch on the First light*)
		SWITCH_SECOND_LIGHT, (*Switch on the Second light*)
		SWITCH_THIRD_LIGHT, (*Switch on the Third light*)
		SWITCH_OFF (*Switch Off light*)
		);
END_TYPE
