(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Program: ErrorHandPrg
 * File: ErrorHandPrg.st
 * Author: carrettam
 * Created: May 07, 2014
 ********************************************************************
 * Implementation of program ErrorHandPrg
 ********************************************************************)

PROGRAM _INIT
	
	//Inizialize value
	//Number of active alarm
	gVisu.ErrorHand.NumbActiveAlm := 0;
	//Active alarm
	ActiveAlm := 0;
	//Internal index for browsing
	i := 0;
	//Alarm Presence
	gVisu.ErrorHand.AlarmPres := 0;
	
	
	//Reset All alarms and the acknoledge image
	FOR i:=0 TO 50 DO
		gVisu.ErrorHand.ETAlarmImg[i] := 0;
		gVisu.ErrorHand.ETAcknoledgeImg[i] := 0;
	END_FOR;
	
END_PROGRAM


PROGRAM _CYCLIC

	////////////////////////////////////////////////////////////////////////////////
	// ERROR HANDLING (INTERFACE BETWEEN CPU ALARM SYSTEM AND HMI ALLARM SYSTEM)
	////////////////////////////////////////////////////////////////////////////////

	//Browse inside the error and ackoledge image for check if the errors are acknoledge
	FOR i:=0 TO 49 DO
		//Check the acknoledge list and update status of alarms
		IF ((gVisu.ErrorHand.ETAlarmImg[i] = 1) AND (gVisu.ErrorHand.ETAcknoledgeImg[i] = 0)) THEN
			//Reset the ackoledged alarms
			gVisu.ErrorHand.ETAlarmImg[i] := 0;
		END_IF

		//Looking for alarm and how many alarm are present
		IF (gVisu.ErrorHand.ETAlarmImg[i] = 1) THEN
			//Increase the number of active alarms
			ActiveAlm := ActiveAlm +1;
		END_IF
	END_FOR;

	//Reset Alarm present in case of normal work
	IF (ActiveAlm = 0) THEN
		//Reset alarm presence
		gVisu.ErrorHand.AlarmPres := 0;
		//Reset the active alarm counter
		gVisu.ErrorHand.NumbActiveAlm := 0;
		//Reset the internal index
		i := 0; 
		
		//If alarm set alarm present and show how many alarm present
	ELSE
		//Write the number of active alarms in the global structure
		gVisu.ErrorHand.NumbActiveAlm := ActiveAlm;
		//Reset the number of active alarms
		ActiveAlm := 0;
		//Set the alarm presence
		gVisu.ErrorHand.AlarmPres := TRUE;
		//Reset index
		i := 0;
	END_IF

END_PROGRAM

PROGRAM _EXIT
	
END_PROGRAM
