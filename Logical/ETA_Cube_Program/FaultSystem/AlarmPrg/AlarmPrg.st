(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Program: AlarmPrg
 * File: AlarmPrg.st
 * Author: carrettam
 * Created: July 01, 2014
 ********************************************************************
 * Implementation of program AlarmPrg
 ********************************************************************)

PROGRAM _INIT

	
END_PROGRAM


PROGRAM _CYCLIC

	////////////////////////////////////////////////////////////////////////////////
	// ALLARM DETECTION
	////////////////////////////////////////////////////////////////////////////////
	
	//Detect slave fault or internal error by reading the Module status
	IF (gEXTIOModule.ModuleStatus.AIstatusModule = 0) THEN
		//If fault then set the alarm
		gVisu.ErrorHand.ETAlarmImg[0] := TRUE;
	ELSE gVisu.ErrorHand.ETAlarmImg[0] := FALSE;
	END_IF
	
	//Detect slave fault or internal error by reading the Module status
	IF (gEXTIOModule.ModuleStatus.ATstatusModule = 0) THEN
		//If fault then set the alarm
		gVisu.ErrorHand.ETAlarmImg[1] := TRUE;
	ELSE gVisu.ErrorHand.ETAlarmImg[1] := FALSE;
	END_IF

	//Detect slave fault or internal error by reading the Module status
	IF (gEXTIOModule.ModuleStatus.BCstatusModule = 0) THEN
		//If fault then set the alarm
		gVisu.ErrorHand.ETAlarmImg[2] := TRUE;
	ELSE gVisu.ErrorHand.ETAlarmImg[2] := FALSE;
	END_IF
	
	//Detect slave fault or internal error by reading the Module status
	IF (gEXTIOModule.ModuleStatus.DCstatusModule = 0) THEN
		//If fault then set the alarm
		gVisu.ErrorHand.ETAlarmImg[3] := TRUE;
	ELSE gVisu.ErrorHand.ETAlarmImg[3] := FALSE;
	END_IF
	
	//Detect slave fault or internal error by reading the Module status
	IF (gEXTIOModule.ModuleStatus.AOstatusModule = 0) THEN
		//If fault then set the alarm
		gVisu.ErrorHand.ETAlarmImg[4] := TRUE;
	ELSE gVisu.ErrorHand.ETAlarmImg[4] := FALSE;
	END_IF
	
	//Detect slave fault or internal error by reading the Module status
	IF (gEXTIOModule.ModuleStatus.DOstatusModule = 0) THEN
		//If fault then set the alarm
		gVisu.ErrorHand.ETAlarmImg[5] := TRUE;
	ELSE gVisu.ErrorHand.ETAlarmImg[5] := FALSE;
	END_IF
	
	//Detect slave fault or internal error by reading the Module status
	IF (gEXTIOModule.ModuleStatus.PSstatusModule = 0) THEN
		//If fault then set the alarm
		gVisu.ErrorHand.ETAlarmImg[6] := TRUE;
	ELSE gVisu.ErrorHand.ETAlarmImg[6] := FALSE;
	END_IF
	
	
	
	
	//Detect slave fault or internal error by reading the Module status
	IF (gX20IOModule.ModuleStatus.BTstatusModule = 0) THEN
		//If fault then set the alarm
		gVisu.ErrorHand.ETAlarmImg[10] := TRUE;
	ELSE gVisu.ErrorHand.ETAlarmImg[10] := FALSE;
	END_IF
	
	//Detect slave fault or internal error by reading the Module status
	IF (gX20IOModule.ModuleStatus.DIstatusModule = 0) THEN
		//If fault then set the alarm
		gVisu.ErrorHand.ETAlarmImg[11] := TRUE;
	ELSE gVisu.ErrorHand.ETAlarmImg[11] := FALSE;
	END_IF
	
	//Detect slave fault or internal error by reading the Module status
	IF (gX20IOModule.ModuleStatus.DOstatusModule = 0) THEN
		//If fault then set the alarm
		gVisu.ErrorHand.ETAlarmImg[12] := TRUE;
	ELSE gVisu.ErrorHand.ETAlarmImg[12] := FALSE;
	END_IF
	
	
	
	
	//Detect slave fault or internal error by reading the Module status
	IF (gXPIOModule.ModuleStatus.XPstatusModule = 0) THEN
		//If fault then set the alarm
		gVisu.ErrorHand.ETAlarmImg[15] := TRUE;
	ELSE gVisu.ErrorHand.ETAlarmImg[15] := FALSE;
	END_IF
	
	
	
	

END_PROGRAM
