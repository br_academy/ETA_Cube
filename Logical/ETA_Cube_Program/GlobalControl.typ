(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * File: Global.typ
 * Author: carrettam
 * Created: June 12, 2014
 ********************************************************************
 * Global data types of project ETATest
 ********************************************************************)

TYPE
	X20IOModule_type : 	STRUCT  (*X20 IO modules connect in the CPU rail*)
		ModuleStatus : X20IOModuleStatus_type; (*X20 IO modules Status of devices*)
		DOut : X20IOModuleDOut_type; (*X20 IO modules Digital output*)
		DInp : X20IOModuleDInp_type; (*X20 IO modules Digital input*)
	END_STRUCT;
	XPIOModule_type : 	STRUCT  (*XP IO modules (Lighting buttons device)*)
		ModuleStatus : XPIOModuleStatus_type; (*XP IO modules status if devices*)
		DOut : XPIOModuleDOut_type; (*XP IO modules digital output*)
		DInp : XPIOModuleDInp_type; (*XP IO modules digital input*)
	END_STRUCT;
	EXTIOModule_type : 	STRUCT  (*EXT IO modules (External IO module connected with the simulation plant)*)
		ModuleStatus : EXTIOModuleStatus_type; (*EXT IO modules Module status*)
		DOut : EXTIOModuleDOut_type; (*EXT IO modules Digital output*)
		AOut : EXTIOModuleAOut_type; (*EXT IO modules Digital input*)
		ATemp : EXTIOModuleATemp_type; (*EXT IO modules Temperature sensor*)
		Encorder : EXTIOModuleEncoder_type; (*EXT IO modules Encoder*)
		AInp : EXTIOModuleAInp_type; (*EXT IO modules Analog input*)
	END_STRUCT;
	gHMISensorColor_type : 	STRUCT  (*HMI sensor color variables (Change the color of the device in scada application)*)
		X20DIswitch : ARRAY[1..8]OF USINT; (*X20DI switch color array*)
		sXPColorLamp01 : WSTRING[80]; (*XP lamp 01 color ***changed to string****)
		uXPColorLamp01 : USINT;
		sXPColorLamp02 : WSTRING[80]; (*XP lamp 02 color ***changed to string****)
		uXPColorLamp02 : USINT;
		sXPColorLamp03 : WSTRING[80]; (*XP lamp 03 color ***changed to string****)
		uXPColorLamp03 : USINT;
		sXPColorLamp04 : WSTRING[80]; (*XP lamp 04 color ***changed to string****)
		uXPColorLamp04 : USINT;
	END_STRUCT;
	gHMIsigSim_type : 	STRUCT  (*HMI signal simulation (all signal forced by PLC)*)
		XPDIswitch : ARRAY[1..4]OF BOOL; (*Digital input switch array*)
		X20DIswitch : ARRAY[1..8]OF BOOL;
		EXTAnalogOut01 : REAL; (*EXT module Analog Output 01*)
		EXTAnalogOut02 : REAL; (*EXT module Analog Output 02*)
	END_STRUCT;
	gRFIDModule_type : 	STRUCT  (*RFID module variables*)
		RFIDReadOk : BOOL; (*RFID read OK*)
		ModuleDisconected : BOOL; (*RFID device disconnected*)
		RFIDlayer : USINT; (*RFID layer in HMI*)
	END_STRUCT;
	X20IOModuleStatus_type : 	STRUCT  (*X20 IO modules Status of devices*)
		DOstatusModule : BOOL; (*Digital output module status*)
		BTstatusModule : BOOL; (*Bus trasmitter module status*)
		DIstatusModule : BOOL; (*Digital input module status*)
	END_STRUCT;
	X20IOModuleDOut_type : 	STRUCT  (*X20 IO modules Digital output*)
		DOled : ARRAY[1..8]OF BOOL;
	END_STRUCT;
	X20IOModuleDInp_type : 	STRUCT  (*X20 IO modules Digital input*)
		DIswitch : ARRAY[1..8]OF BOOL; (*Digital input switch array*)
	END_STRUCT;
	XPIOModuleStatus_type : 	STRUCT  (*XP IO modules status if devices*)
		XPstatusModule : BOOL; (*XP modules status*)
	END_STRUCT;
	XPIOModuleDOut_type : 	STRUCT  (*XP IO modules digital output*)
		DOledGreen01 : BOOL; (*Digital output led green first lamp*)
		DOledYellow01 : BOOL; (*Digital output led Yellow first lamp*)
		DOledRed01 : BOOL; (*Digital output led Red first lamp*)
		DOledGreen02 : BOOL; (*Digital output led green Second lamp*)
		DOledYellow02 : BOOL; (*Digital output led Yellow Second lamp*)
		DOledRed02 : BOOL; (*Digital output led Red Second lamp*)
		DOledGreen03 : BOOL; (*Digital output led green Third lamp*)
		DOledYellow03 : BOOL; (*Digital output led Yellow Third lamp*)
		DOledRed03 : BOOL; (*Digital output led Red Third lamp*)
		DOledGreen04 : BOOL; (*Digital output led green Fourth lamp*)
		DOledYellow04 : BOOL; (*Digital output led Yellow Fourth lamp*)
		DOledRed04 : BOOL; (*Digital output led Red Fourth lamp*)
	END_STRUCT;
	XPIOModuleDInp_type : 	STRUCT  (*XP IO modules digital input*)
		DIswitch : ARRAY[1..4]OF BOOL; (*Digital input switch array*)
	END_STRUCT;
	EXTIOModuleStatus_type : 	STRUCT  (*EXT IO modules Module status*)
		PSstatusModule : BOOL; (*Power Supply module status*)
		ATstatusModule : BOOL; (*Analog temperature module status*)
		AOstatusModule : BOOL; (*Analog Output module status*)
		AIstatusModule : BOOL; (*Analog input module status*)
		DCstatusModule : BOOL; (*enconder module status*)
		DOstatusModule : BOOL; (*Digital output module status*)
		BCstatusModule : BOOL; (*Remote bus controller module status*)
	END_STRUCT;
	EXTIOModuleEncoder_type : 	STRUCT  (*EXT IO modules Encoder*)
		Encoder01 : INT; (*Encoder 01*)
		DigitalInput02 : BOOL;
		Encoder02 : DINT;
		DigitalInput01 : USINT; (*Digital Input 01*)
	END_STRUCT;
	EXTIOModuleDOut_type : 	STRUCT  (*EXT IO modules Digital output*)
		Cooling : BOOL; (*Cooling device output*)
		Heating : BOOL; (*Heating device output*)
	END_STRUCT;
	EXTIOModuleAOut_type : 	STRUCT  (*EXT IO modules Digital input*)
		AnalogDisplay01 : INT; (*Analog Display 01*)
		AnalogDisplay02 : INT; (*Analog Display 02*)
	END_STRUCT;
	EXTIOModuleATemp_type : 	STRUCT  (*EXT IO modules Temperature sensor*)
		SimTemp : INT; (*Temperature in simulation plant*)
	END_STRUCT;
	EXTIOModuleAInp_type : 	STRUCT  (*EXT IO modules Analog input*)
		trimmer01 : INT; (*Trimmer 01*)
		trimmer02 : INT; (*Trimmer 02*)
		trimmer01_deg : REAL; (*Trimmer 01 with value in degrees used in paper widget*)
		trimmer02_deg : REAL; (*Trimmer 02 with value in degrees used in paper widget*)
	END_STRUCT;
	Infotainment_type : 	STRUCT  (*ADDED*)
		Automatic : InfotainmentAuto_type;
		Manual : InfotainmentManual_type;
	END_STRUCT;
	InfotainmentAuto_type : 	STRUCT 
		enable : ARRAY[0..gMAX_CLIENTS]OF BOOL := [8(TRUE)];
		step : ARRAY[0..gMAX_CLIENTS]OF USINT := [8(0)];
		Banner : Banner_type; (*ADDED*)
		Dialog : Dialog_type;
	END_STRUCT;
	Banner_type : 	STRUCT 
		Timer : Timer_type;
		startCycle : ARRAY[0..gMAX_CLIENTS]OF BOOL := [8(TRUE)];
		controlWord : ARRAY[0..gMAX_CLIENTS]OF STRING[80];
		tChangeBanner : DATE_AND_TIME;
		actualNum : ARRAY[0..gMAX_CLIENTS]OF USINT := [8(0)];
	END_STRUCT;
	Dialog_type : 	STRUCT 
		Timer : Timer_type;
		TabControl : TabControl_type;
		openDialog : ARRAY[0..gMAX_CLIENTS]OF BOOL := [8(FALSE)];
		closeDialog : ARRAY[0..gMAX_CLIENTS]OF BOOL := [8(FALSE)];
		doneTON_Delay : ARRAY[0..gMAX_CLIENTS]OF BOOL := [8(FALSE)];
		tOpenDlg : DATE_AND_TIME;
		tPlayVideo : DATE_AND_TIME;
		videoWord : ARRAY[0..gMAX_CLIENTS]OF STRING[80] := [8('Media/SV21_MpAlarmX/Infotainment/MpAlarmX_Subtitles.webm')];
		pdfWord : ARRAY[0..gMAX_CLIENTS]OF STRING[80] := [8('Media/SV21_MpAlarmX/Infotainment/MpAlarmX.pdf')];
		video_pdfFileNum_temp : ARRAY[0..gMAX_CLIENTS]OF USINT := [8(0)];
		video_pdfFileNum : ARRAY[0..gMAX_CLIENTS]OF USINT := [8(0)];
	END_STRUCT;
	TabControl_type : 	STRUCT 
		isTabVideoControl : ARRAY[0..gMAX_CLIENTS]OF BOOL := [8(FALSE)];
		isTabPdfControl : ARRAY[0..gMAX_CLIENTS]OF BOOL := [8(FALSE)];
		videoStart : ARRAY[0..gMAX_CLIENTS]OF BOOL := [8(FALSE)];
		switchPdfPage : ARRAY[0..gMAX_CLIENTS]OF USINT := [8(1)];
	END_STRUCT;
	Timer_type : 	STRUCT  (*ADDED*)
		TON_1 : ARRAY[0..gMAX_CLIENTS]OF TON; (*ADDED*)
		TON_2 : ARRAY[0..gMAX_CLIENTS]OF TON; (*ADDED*)
		TON_Delay : ARRAY[0..gMAX_CLIENTS]OF TON; (*ADDED*)
		cycleNum : ARRAY[0..gMAX_CLIENTS]OF UDINT := [8(0)];
	END_STRUCT;
	InfotainmentManual_type : 	STRUCT 
		waitForCloseFlyOut : ARRAY[0..gMAX_CLIENTS]OF BOOL := [8(FALSE)];
		isOpenFlyOut : ARRAY[0..gMAX_CLIENTS]OF BOOL := [8(FALSE)];
	END_STRUCT;
END_TYPE
