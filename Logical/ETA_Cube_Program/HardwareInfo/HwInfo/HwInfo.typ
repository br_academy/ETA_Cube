(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Program: HwInfo
 * File: HwInfo.typ
 * Author: carrettam
 * Created: May 26, 2014
 ********************************************************************
 * Local data types of program HwInfo
 ********************************************************************)

TYPE
	InfoStep_num : 
		( (*Enumerator for HW info statemachine*)
		STATE_WAIT := 0, (*Wait state*)
		STATE_CREATE_INFO := 10, (*Create info from the target*)
		STATE_GET_HW_INFO_CURRENT := 20, (*Acquire the Info for current target*)
		STATE_GET_HW_INFO_CONFIGURED := 30 (*Acquire the info for the configured target*)
		);
END_TYPE
