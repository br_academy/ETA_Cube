(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Program: HwInfo
 * File: HwInfo.st
 * Author: carrettam
 * Created: May 26, 2014
 ********************************************************************
 * Implementation of program HwInfo
 ********************************************************************)

PROGRAM _INIT

	// Initialize Parameters and statemachine
	HwConfigInfo.Parameter.InfoKind		:= asdiagALL;
	InfoStep							:= STATE_CREATE_INFO;

END_PROGRAM


PROGRAM _CYCLIC

	//Case machine for get info
	CASE InfoStep OF
		
		(***********************************************************************************)
		(*********************************** CREATE INFO ***********************************)
		STATE_CREATE_INFO:	//	Create Info
		
			// Enable FUB
			DiagCreateInfo_0.enable			:= TRUE;
			DiagCreateInfo_0.infoKind		:= HwConfigInfo.Parameter.InfoKind;
			
			// FUB call
			DiagCreateInfo_0();

			// Check FUB status
			IF DiagCreateInfo_0.status = 0 THEN
				// Disable FUB
				DiagCreateInfo_0.enable				:= FALSE;
				// Save Outputs
				HwConfigInfo.Parameter.Ident		:= DiagCreateInfo_0.ident;
				HwConfigInfo.Parameter.NrEntries	:= DiagCreateInfo_0.nrEntries;
				
				// Reset String structure
				FOR Index := 0 TO 30 DO
					FOR Index2:= 0 TO 30 DO
						HwConfigInfo.StringInfoConfigured[Index].Entry[Index2]	 := 0;
						HwConfigInfo.StringInfoCurrent[Index].Entry[Index2]		 := 0; 
					END_FOR;
				END_FOR;
				
				// Change state
				InfoStep		:= STATE_GET_HW_INFO_CURRENT;
			END_IF;
		
		(***********************************************************************************)
		(******************************* GET HW INFO PLUGGED *******************************)
		STATE_GET_HW_INFO_CURRENT:	//	Get String Info current
		
			// Get info for all Entries
			FOR Index := 0 TO HwConfigInfo.Parameter.NrEntries - 1 DO
				// Enable FUB
				DiagGetStrInfo_0.enable			:= TRUE;
				// Set Parameters
				DiagGetStrInfo_0.ident			:= HwConfigInfo.Parameter.Ident;
				DiagGetStrInfo_0.index			:= Index;
				DiagGetStrInfo_0.infoCode		:= asdiagPLUGGED_MODULE;
				DiagGetStrInfo_0.pBuffer		:= ADR(HwConfigInfo.StringInfoCurrent[Index]);
				DiagGetStrInfo_0.bufferLen		:= SIZEOF(HwConfigInfo.StringInfoCurrent[Index]);

				// FUB call
				DiagGetStrInfo_0();	
				
				// Check FUB status
				IF ((DiagGetStrInfo_0.status = 0) AND (Index >= (HwConfigInfo.Parameter.NrEntries - 1))) THEN
					// Disable FUB
					DiagGetStrInfo_0.enable		:= FALSE;
					// Change state
					InfoStep					:= STATE_GET_HW_INFO_CONFIGURED;
				ELSE
					// Disable FUB
					DiagGetStrInfo_0.enable		:= FALSE;
				END_IF;
			END_FOR;
		
		(************************************************************************************)
		(****************************** GET HW INFO CONFIGURED ******************************)
		STATE_GET_HW_INFO_CONFIGURED:	//	Get String Info configured
		
			FOR Index := 0 TO HwConfigInfo.Parameter.NrEntries - 1 DO
				// Enable FUB
				DiagGetStrInfo_0.enable			:= TRUE;
				// Set parameters
				DiagGetStrInfo_0.ident			:= HwConfigInfo.Parameter.Ident;
				DiagGetStrInfo_0.index			:= Index;
				DiagGetStrInfo_0.infoCode		:= asdiagCONFIG_MODULE;
				DiagGetStrInfo_0.pBuffer		:= ADR(HwConfigInfo.StringInfoConfigured[Index]);
				DiagGetStrInfo_0.bufferLen		:= SIZEOF(HwConfigInfo.StringInfoConfigured[Index]);
				
				// FUB call
				DiagGetStrInfo_0();	
				
				// Check FUB status
				IF ((DiagGetStrInfo_0.status = 0) AND (Index >= (HwConfigInfo.Parameter.NrEntries - 1))) THEN
					
					// Disable FUB
					DiagGetStrInfo_0.enable											:= FALSE;

					// Compare the configured with the plugged hardware
					IF  (brsstrcmp(ADR(HwConfigInfo.StringInfoCurrent[0].Entry), ADR(HwConfigInfo.StringInfoConfigured[0].Entry)) = 0) AND
						(brsstrcmp(ADR(HwConfigInfo.StringInfoCurrent[1].Entry), ADR(HwConfigInfo.StringInfoConfigured[1].Entry)) = 0) AND
						//(brsstrcmp(ADR(HwConfigInfo.StringInfoCurrent[2].Entry), ADR(HwConfigInfo.StringInfoConfigured[2].Entry)) <> 0) AND
						(brsstrcmp(ADR(HwConfigInfo.StringInfoCurrent[3].Entry), ADR(HwConfigInfo.StringInfoConfigured[3].Entry)) = 0) AND
						(brsstrcmp(ADR(HwConfigInfo.StringInfoCurrent[4].Entry), ADR(HwConfigInfo.StringInfoConfigured[4].Entry)) = 0) AND
						(brsstrcmp(ADR(HwConfigInfo.StringInfoCurrent[5].Entry), ADR(HwConfigInfo.StringInfoConfigured[5].Entry)) = 0) AND
						(brsstrcmp(ADR(HwConfigInfo.StringInfoCurrent[6].Entry), ADR(HwConfigInfo.StringInfoConfigured[6].Entry)) = 0) AND
						(brsstrcmp(ADR(HwConfigInfo.StringInfoCurrent[7].Entry), ADR(HwConfigInfo.StringInfoConfigured[7].Entry)) = 0) AND
						(brsstrcmp(ADR(HwConfigInfo.StringInfoCurrent[8].Entry), ADR(HwConfigInfo.StringInfoConfigured[8].Entry)) = 0) AND
						(brsstrcmp(ADR(HwConfigInfo.StringInfoCurrent[9].Entry), ADR(HwConfigInfo.StringInfoConfigured[9].Entry)) = 0) AND
						(brsstrcmp(ADR(HwConfigInfo.StringInfoCurrent[10].Entry), ADR(HwConfigInfo.StringInfoConfigured[10].Entry)) = 0) AND
						(brsstrcmp(ADR(HwConfigInfo.StringInfoCurrent[11].Entry), ADR(HwConfigInfo.StringInfoConfigured[11].Entry)) = 0) AND
						(brsstrcmp(ADR(HwConfigInfo.StringInfoCurrent[12].Entry), ADR(HwConfigInfo.StringInfoConfigured[12].Entry)) = 0) AND
						(brsstrcmp(ADR(HwConfigInfo.StringInfoCurrent[13].Entry), ADR(HwConfigInfo.StringInfoConfigured[13].Entry)) = 0) AND
						(brsstrcmp(ADR(HwConfigInfo.StringInfoCurrent[14].Entry), ADR(HwConfigInfo.StringInfoConfigured[14].Entry)) = 0) AND
						(brsstrcmp(ADR(HwConfigInfo.StringInfoCurrent[15].Entry), ADR(HwConfigInfo.StringInfoConfigured[15].Entry)) = 0) AND
						(brsstrcmp(ADR(HwConfigInfo.StringInfoCurrent[16].Entry), ADR(HwConfigInfo.StringInfoConfigured[16].Entry)) = 0) AND
						(brsstrcmp(ADR(HwConfigInfo.StringInfoCurrent[17].Entry), ADR(HwConfigInfo.StringInfoConfigured[17].Entry)) = 0) AND
						(brsstrcmp(ADR(HwConfigInfo.StringInfoCurrent[18].Entry), ADR(HwConfigInfo.StringInfoConfigured[18].Entry)) = 0) AND
						(brsstrcmp(ADR(HwConfigInfo.StringInfoCurrent[19].Entry), ADR(HwConfigInfo.StringInfoConfigured[19].Entry)) = 0) AND
						(brsstrcmp(ADR(HwConfigInfo.StringInfoCurrent[20].Entry), ADR(HwConfigInfo.StringInfoConfigured[20].Entry)) = 0) THEN
						HwConfigInfo.Status.ConfigError				:= FALSE;
					ELSE
						HwConfigInfo.Status.ConfigError				:= TRUE;
					END_IF;
					

					// Copy string of plugged hardware to global structure
					brsstrcpy(ADR(HwConfigInfo.Status.StrPluggedHW[0]), ADR(HwConfigInfo.StringInfoCurrent[0].Entry));
					brsstrcpy(ADR(HwConfigInfo.Status.StrPluggedHW[1]), ADR(HwConfigInfo.StringInfoCurrent[1].Entry));
					brsstrcpy(ADR(HwConfigInfo.Status.StrPluggedHW[2]), ADR(HwConfigInfo.StringInfoCurrent[2].Entry));
					brsstrcpy(ADR(HwConfigInfo.Status.StrPluggedHW[3]), ADR(HwConfigInfo.StringInfoCurrent[3].Entry));
					brsstrcpy(ADR(HwConfigInfo.Status.StrPluggedHW[4]), ADR(HwConfigInfo.StringInfoCurrent[4].Entry));
					brsstrcpy(ADR(HwConfigInfo.Status.StrPluggedHW[5]), ADR(HwConfigInfo.StringInfoCurrent[5].Entry));
					brsstrcpy(ADR(HwConfigInfo.Status.StrPluggedHW[6]), ADR(HwConfigInfo.StringInfoCurrent[6].Entry));
					brsstrcpy(ADR(HwConfigInfo.Status.StrPluggedHW[7]), ADR(HwConfigInfo.StringInfoCurrent[7].Entry));
					brsstrcpy(ADR(HwConfigInfo.Status.StrPluggedHW[8]), ADR(HwConfigInfo.StringInfoCurrent[8].Entry));
					brsstrcpy(ADR(HwConfigInfo.Status.StrPluggedHW[9]), ADR(HwConfigInfo.StringInfoCurrent[9].Entry));
					brsstrcpy(ADR(HwConfigInfo.Status.StrPluggedHW[10]), ADR(HwConfigInfo.StringInfoCurrent[10].Entry));
					brsstrcpy(ADR(HwConfigInfo.Status.StrPluggedHW[11]), ADR(HwConfigInfo.StringInfoCurrent[11].Entry));
					brsstrcpy(ADR(HwConfigInfo.Status.StrPluggedHW[12]), ADR(HwConfigInfo.StringInfoCurrent[12].Entry));
					brsstrcpy(ADR(HwConfigInfo.Status.StrPluggedHW[13]), ADR(HwConfigInfo.StringInfoCurrent[13].Entry));
					brsstrcpy(ADR(HwConfigInfo.Status.StrPluggedHW[14]), ADR(HwConfigInfo.StringInfoCurrent[14].Entry));
					brsstrcpy(ADR(HwConfigInfo.Status.StrPluggedHW[15]), ADR(HwConfigInfo.StringInfoCurrent[15].Entry));
					brsstrcpy(ADR(HwConfigInfo.Status.StrPluggedHW[16]), ADR(HwConfigInfo.StringInfoCurrent[16].Entry));
					brsstrcpy(ADR(HwConfigInfo.Status.StrPluggedHW[17]), ADR(HwConfigInfo.StringInfoCurrent[17].Entry));
					brsstrcpy(ADR(HwConfigInfo.Status.StrPluggedHW[18]), ADR(HwConfigInfo.StringInfoCurrent[18].Entry));
					brsstrcpy(ADR(HwConfigInfo.Status.StrPluggedHW[19]), ADR(HwConfigInfo.StringInfoCurrent[19].Entry));
					brsstrcpy(ADR(HwConfigInfo.Status.StrPluggedHW[20]), ADR(HwConfigInfo.StringInfoCurrent[20].Entry));
					
					// Copy string of configrued hardware to global structure
					brsstrcpy(ADR(HwConfigInfo.Status.StrConfiguredHW[0]), ADR(HwConfigInfo.StringInfoConfigured[0].Entry));
					brsstrcpy(ADR(HwConfigInfo.Status.StrConfiguredHW[1]), ADR(HwConfigInfo.StringInfoConfigured[1].Entry));
					brsstrcpy(ADR(HwConfigInfo.Status.StrConfiguredHW[2]), ADR(HwConfigInfo.StringInfoConfigured[2].Entry));
					brsstrcpy(ADR(HwConfigInfo.Status.StrConfiguredHW[3]), ADR(HwConfigInfo.StringInfoConfigured[3].Entry));
					brsstrcpy(ADR(HwConfigInfo.Status.StrConfiguredHW[4]), ADR(HwConfigInfo.StringInfoConfigured[4].Entry));
					brsstrcpy(ADR(HwConfigInfo.Status.StrConfiguredHW[5]), ADR(HwConfigInfo.StringInfoConfigured[5].Entry));
					brsstrcpy(ADR(HwConfigInfo.Status.StrConfiguredHW[6]), ADR(HwConfigInfo.StringInfoConfigured[6].Entry));
					brsstrcpy(ADR(HwConfigInfo.Status.StrConfiguredHW[7]), ADR(HwConfigInfo.StringInfoConfigured[7].Entry));
					brsstrcpy(ADR(HwConfigInfo.Status.StrConfiguredHW[8]), ADR(HwConfigInfo.StringInfoConfigured[8].Entry));
					brsstrcpy(ADR(HwConfigInfo.Status.StrConfiguredHW[9]), ADR(HwConfigInfo.StringInfoConfigured[9].Entry));
					brsstrcpy(ADR(HwConfigInfo.Status.StrConfiguredHW[10]), ADR(HwConfigInfo.StringInfoConfigured[10].Entry));
					brsstrcpy(ADR(HwConfigInfo.Status.StrConfiguredHW[11]), ADR(HwConfigInfo.StringInfoConfigured[11].Entry));
					brsstrcpy(ADR(HwConfigInfo.Status.StrConfiguredHW[12]), ADR(HwConfigInfo.StringInfoConfigured[12].Entry));
					brsstrcpy(ADR(HwConfigInfo.Status.StrConfiguredHW[13]), ADR(HwConfigInfo.StringInfoConfigured[13].Entry));
					brsstrcpy(ADR(HwConfigInfo.Status.StrConfiguredHW[14]), ADR(HwConfigInfo.StringInfoConfigured[14].Entry));
					brsstrcpy(ADR(HwConfigInfo.Status.StrConfiguredHW[15]), ADR(HwConfigInfo.StringInfoConfigured[15].Entry));
					brsstrcpy(ADR(HwConfigInfo.Status.StrConfiguredHW[16]), ADR(HwConfigInfo.StringInfoConfigured[16].Entry));
					brsstrcpy(ADR(HwConfigInfo.Status.StrConfiguredHW[17]), ADR(HwConfigInfo.StringInfoConfigured[17].Entry));
					brsstrcpy(ADR(HwConfigInfo.Status.StrConfiguredHW[18]), ADR(HwConfigInfo.StringInfoConfigured[18].Entry));
					brsstrcpy(ADR(HwConfigInfo.Status.StrConfiguredHW[19]), ADR(HwConfigInfo.StringInfoConfigured[19].Entry));
					brsstrcpy(ADR(HwConfigInfo.Status.StrConfiguredHW[20]), ADR(HwConfigInfo.StringInfoConfigured[20].Entry));
					
					// Change state
					InfoStep					:= STATE_CREATE_INFO;
				ELSE
					// Disable FUB
					DiagGetStrInfo_0.enable		:= FALSE;
				END_IF;
			END_FOR;
	END_CASE;	
	
	
END_PROGRAM
