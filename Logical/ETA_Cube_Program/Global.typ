(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * File: Global.typ
 * Author: carrettam
 * Created: June 12, 2014
 ********************************************************************
 * Global data types of project ETATest
 ********************************************************************)
(*------------------------------------------------------------------------------------------------- HARDWARE CONFIGURATION --------------------------------------------------------------------------------------------------------*)

TYPE
	HwConfigInfo_type : 	STRUCT  (*Structure for hardware configuration*)
		Parameter : HwConfigParameter_type; (*Hardware Parameters*)
		Status : HwConfigStatus_type; (*Hardware Status*)
		StringInfoConfigured : ARRAY[0..30]OF HwConfigStrInfo_type; (*Info string hardware configuration*)
		StringInfoCurrent : ARRAY[0..30]OF HwConfigStrInfo_type; (*Info string current configuration*)
	END_STRUCT;
	HwConfigParameter_type : 	STRUCT  (*Structure for Parameters for hardware config*)
		InfoKind : UDINT; (*Information type*)
		NrEntries : UDINT; (*Number of entries*)
		Ident : UDINT; (*Ident of created info*)
	END_STRUCT;
	HwConfigStatus_type : 	STRUCT  (*Structure for status of hardware config*)
		CurrentConfigType : USINT; (*Current config type*)
		ConfigError : BOOL; (*True if plugged config is not matching any configured*)
		StrConfiguredHW : ARRAY[0..20]OF STRING[80]; (*String array for hardware info*)
		StrPluggedHW : ARRAY[0..20]OF STRING[80]; (*String array for hardware info*)
	END_STRUCT;
	HwConfigStrInfo_type : 	STRUCT  (*Structure for hardware info (string)*)
		Entry : ARRAY[0..30]OF USINT; (*Variable for one letter*)
	END_STRUCT;
END_TYPE

(*--------------------------------------------------------------------------------------------------------------- VISUALIZATION ---------------------------------------------------------------------------------------------------------------------*)

TYPE
	Visu_type : 	STRUCT  (*Visualization tags*)
		ChangePage : USINT; (*Change page on visualization*)
		CurrentPage : USINT; (*Current page on visualization*)
		ErrorHand : ErrorHand_type; (*Error handling variable*)
		SV20_UserPage : SV20_UserPage_type;
		userValidCount : USINT := 0; (*Number of valid users connecting to mapp View*)
		ClientInfo : ARRAY[0..gMAX_CLIENTS]OF ClientInfo_type; (*Mapp View content caching structure*)
	END_STRUCT;
	ErrorHand_type : 	STRUCT  (*Error handling*)
		AlarmPres : BOOL; (*Allarm presence*)
		NumbActiveAlm : USINT; (*Number of active allarm*)
		ETAcknoledgeImg : ARRAY[0..50]OF BOOL; (*ETA Acknoledge image*)
		ETAlarmImg : ARRAY[0..50]OF BOOL; (*ETA Alarm image*)
	END_STRUCT;
END_TYPE

(*--------------------------------------------------------------------------------------------------------------------- MOTION ----------------------------------------------------------------------------------------------------------------------------*)

TYPE
	Error_enum : 
		( (*Axis Error enum for ERROR Case*)
		BAD_REQUEST := 29238,
		LAG_ERROR := 4007,
		HOMING_ERROR := 5018,
		NO_ERROR := 0,
		COMMUNICATION_LOST := 29265,
		BREAKDOWN := 1012,
		UNPLUGGED := 32189
		);
	Move1_enum : 
		( (*First Movement type*)
		INIT_ACTION, (*Initialze + Negative movement*)
		ACTION1, (*Positive movement*)
		ACTION2 (*Negative movement*)
		);
	Move2_enum : 
		( (*Second Movement type*)
		INIT_DIR, (*Homing to zero position*)
		DIR1, (*position 1*)
		DIR2, (*position 2*)
		DIR3, (*position 3*)
		DIR4 (*position 4*)
		);
	motion_axis_enum : 
		( (*main switch for axis and movement*)
		DISCONNECTED, (*ACOPOS is disconnected*)
		INIT, (*Waiting on ReadyToPowerOn*)
		POWER_ON, (*Turn on controller*)
		HOMING, (*Set up Home position*)
		STANDSTILL, (*Init position for Moves*)
		MOVE1, (*Motor just accelerating and decelerating*)
		MOVE2, (*Motor positon change by absolut movement into 4 directiories*)
		MOVE3, (*In this movement motor do nothing*)
		ERROR (*Error state*)
		);
END_TYPE

(*--------------------------------------------------------------------------------------------------------------------- USER MANAGEMENT----------------------------------------------------------------------------------------------------------------------------*)

TYPE
	SV20_UserPage_type : 	STRUCT 
		MpUserXLogin_0 : MpUserXLogin;
		MpUserXManagerUI_0 : MpUserXManagerUI;
		MpUserXLoginUIConnect : MpUserXLoginUIConnectType;
		MpUserXManagerUIConnect : MpUserXMgrUIConnectType;
		MpUserXLoginUI_0 : MpUserXLoginUI;
		userList : ARRAY[0..19]OF WSTRING[70];
		roleDropDown : ARRAY[0..9]OF WSTRING[70];
		slotIdRights : USINT;
		ErrorMessage : STRING[100];
	END_STRUCT;
	ClientInfo_type : 	STRUCT 
		isValid : BOOL;
		userId : STRING[80];
		ipAddress : STRING[20];
		languageId : STRING[20];
		slotId : USINT;
		currentPageId : STRING[20];
	END_STRUCT;
END_TYPE
