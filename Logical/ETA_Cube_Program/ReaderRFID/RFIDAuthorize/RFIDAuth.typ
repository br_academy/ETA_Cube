(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * PROGRAM: RFIDAuthorize
 * File: RFIDAuthorize.typ
 * Author: Bubenik
 * Created: April 1, 2016
 ********************************************************************
 * Implementation OF PROGRAM RFIDAuthorize
 ********************************************************************)

TYPE
	RFID_NAME : 	STRUCT  (*Structure for database*)
		valid : BOOL; (*Status if place in array is avaible*)
		ID : STRING[16]; (*ID from chip*)
		Name : STRING[20]; (*Name of person*)
		Key : STRING[8]; (*Password*)
		Rights : USINT; (*Rights in settings*)
	END_STRUCT;
	authtorization_enum : 
		( (*Authorization case*)
		NEW_CONNECT, (*find keyword *)
		USER, (*compare if person is in database by ID and valid*)
		PASS, (*compare password*)
		FULL_RIGHTS,
		HALF_RIGHTS,
		NO_RIGHTS
		);
END_TYPE
