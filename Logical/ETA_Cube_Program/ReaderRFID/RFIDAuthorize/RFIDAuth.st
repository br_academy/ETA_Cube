(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * PROGRAM: RFIDAuthorize
 * File: RFIDAuthorize.st
 * Author: Bubenik
 * Created: April 1, 2016
 ********************************************************************
 * Implementation OF PROGRAM RFIDAuthorize
 ********************************************************************)

PROGRAM _INIT
	i:=0;
	Demo_Startup := 0;
	Potentiomer_Sensitivity := 0;
	Time_And_Date := 0;
	Allow_Control_Parts := 0;
	Demo_LED_Speed := 0;
	Demo_XP_Light_Speed := 0;
	strcpy(ADR(Tag_ID), ADR('Disconnected'));
	RFID_status_colour := 0;
	Allow_RFID := 1;
END_PROGRAM

PROGRAM _CYCLIC

	IF Allow_RFID = 1 THEN																			//Allows right and authorization by RFID
	
		(*Authorization when RFID chip attached to the RFID Reader*)
		CASE authorization OF 
		
			NEW_CONNECT:
				Demo_Startup := 1;																	//turn off all settings
				Allow_rights :=1;
				Potentiomer_Sensitivity := 1;
				Time_And_Date := 1;
				Allow_Control_Parts := 1;
				Demo_LED_Speed := 1;
				Demo_XP_Light_Speed := 1;
				
				memcpy(ADR(data), ADR(ReadData), 11);												//Copy 11 letter from RFID
				IF (brsstrcmp(ADR(data),ADR('PiccSelect:'))=0) THEN									//Compare if the message from RFID reader is about new attached chip
					memcpy(ADR(ID),ADR(ReadData)+12, 8);											//Copy ID from PiccSelect message
					strcpy(ADR(Tag_ID), ADR(ID));				
					strcpy(ADR(ReadData), ADR(''));													//Clean up ReadData
					usbAction := USB_READ;															//Change phase to read password
					authorization := USER;															//Change phase
				END_IF;
			
			USER:	
				FOR i := 0 TO 9 BY 1 DO																//Finding Client in Structure
					IF ((RFID_Name[i].valid = TRUE) AND (brsstrcmp(ADR(RFID_Name[i].ID),ADR(ID))=0)) THEN	//finding by .valid status and saved ID																		
						authorization := PASS;														//if find person in database change phase
						EXIT;																		//if find can exit
					ELSE
						authorization := NEW_CONNECT;												//if not find person change phase to wait on chip
					END_IF;
				END_FOR;
			
			PASS:	
				TON_waitPass(IN := TRUE, PT := 10);													//time for response from RFID on password request
				IF TON_waitPass.Q THEN																
					memcpy(ADR(password),ADR(ID),8);												//copy password from chip to variable
					TON_waitPass(IN := FALSE, PT := 10);											//Reset timer
					IF brsstrcmp(ADR(RFID_Name[i].Key),ADR(password))=0 THEN						//Compare password in database and password from chip
						Allow_rights := 0;															//if password is correct allow rights
						strcpy(ADR(RFID_Header_Name), ADR(RFID_Name[i].Name));						//Person's name from database is written to Visu
						IF (RFID_Name[i].Rights = 1) THEN											//compare rights of person
							authorization := HALF_RIGHTS;											//change phase to half rights
						ELSIF (RFID_Name[i].Rights = 2) THEN										//compare rights of person
							authorization := FULL_RIGHTS;											//change phase to full rights
						ELSE authorization := NO_RIGHTS;											//change phase to no rights but person is in database
						END_IF;
					ELSE
						Allow_rights := 1;															//disable rights
						authorization := NEW_CONNECT;												//if password is wrong change phase to wait on chip
					END_IF;	
					strcpy(ADR(ID), ADR(''));														//reset ID variable
					strcpy(ADR(password), ADR(''));													//reset password variable
					strcpy(ADR(data), ADR(''));														//reset data
					strcpy(ADR(ReadData), ADR(''));													//reset ReadData
				END_IF;
	
			FULL_RIGHTS:
				Rights_colour := 2;																	//Red color of Exit button in Visu
				Demo_Startup := 0;																	//Can change Demo time 
				Potentiomer_Sensitivity := 0;														//Can change potentiomer sensitivity
				Time_And_Date := 0;																	//Can change Time and Date
				Allow_Control_Parts := 0;															//Can change what mode can run
				Demo_LED_Speed := 0;																//Can change speed of LEDs in Demo
				Demo_XP_Light_Speed := 0;															//Can change XP lamp Speed in Demo
				IF Allow_rights = 1 THEN															
					authorization := NEW_CONNECT;													//if person hit Exit button change phase to find chip
				END_IF;
		
			HALF_RIGHTS:
				Rights_colour := 1;																	//Blue color of Exit button in Visu
				Demo_Startup := 0;																	//Can change Demo time 
				Potentiomer_Sensitivity := 0;														//Can change potentiomer sensitivity
				Time_And_Date := 1;																	//Cannot change Time and Date
				Allow_Control_Parts := 1;															//Cannot change what mode can run
				Demo_LED_Speed := 0;																//Can change speed of LEDs in Demo
				Demo_XP_Light_Speed := 0;															//Can change XP lamp Speed in Demo
				IF Allow_rights = 1 THEN
					authorization := NEW_CONNECT;													//if person hit Exit button change phase to find chip
				END_IF;

			NO_RIGHTS:
				Rights_colour := 1;																	//Green color of Exit button in Visu
				Demo_Startup := 1;																	//Cannot change Demo time 
				Potentiomer_Sensitivity := 0;														//Can change potentiomer sensitivity
				Time_And_Date := 1;																	//Cannot change Time and Date
				Allow_Control_Parts := 1;															//Cannot change what mode can run
				Demo_LED_Speed := 1;																//Cannot change speed of LEDs in Demo
				Demo_XP_Light_Speed := 1;															//Cannot change XP lamp Speed in Demo
				IF Allow_rights = 1 THEN
					authorization := NEW_CONNECT;													//if person hit Exit button change phase to find chip
				END_IF;	
		END_CASE;
				
	ELSE
		Demo_Startup := 0;
		Potentiomer_Sensitivity := 0;
		Time_And_Date := 0;
		Allow_Control_Parts := 0;
		Demo_LED_Speed := 0;
		Demo_XP_Light_Speed := 0;
	END_IF;
	
END_PROGRAM
