(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer 
 ********************************************************************
 * PROGRAM: 125kHzTransp
 * File: 125kHzTransp.st
 * Author: Bernecker + Rainer
 * Created: September 15, 2009
 ********************************************************************
 * Description: Supports 5E9020.29 (125kHz) transponder reader
 ********************************************************************)


(********************************************************************
 *  INIT UP
 ********************************************************************)
PROGRAM _INIT
	usbAttachDetachCount := 0;
	usbNodeIx := 0;
	usbAction := USB_GETNODELIST;
	gRFIDModule.RFIDReadOk := FALSE;
END_PROGRAM


(********************************************************************
 *  CYCLIC TASK														*
 ********************************************************************)
PROGRAM _CYCLIC
	
	CASE usbAction OF
		USB_GETNODELIST:
			UsbNodeListGetFub(enable := TRUE,
			pBuffer := ADR(usbNodeList),
			bufferSize := SIZEOF(usbNodeList),
			filterInterfaceClass := 0,
			filterInterfaceSubClass := 0);
		
			IF UsbNodeListGetFub.status = ERR_OK AND UsbNodeListGetFub.listNodes <> 0 THEN
				(* USB Device Attach OR detach *)
				usbAction := USB_SEARCHDEVICE;
				usbNodeIx := 0;
				usbAttachDetachCount := UsbNodeListGetFub.attachDetachCount;
			END_IF
		
		USB_SEARCHDEVICE:
			sizeUsbNode := SIZEOF(usbDevice);
			UsbNodeGetFub(enable := TRUE,
			nodeId := usbNodeList[usbNodeIx],
			pBuffer := ADR(usbDevice),
			bufferSize := SIZEOF(usbDevice));
		
			IF UsbNodeGetFub.status = ERR_OK THEN
				(* USB Prolific Transponder ? *)
				IF usbDevice.vendorId = TRANSPONDER_PROLIFIC_VENDOR_ID
					AND usbDevice.productId = TRANSPONDER_PROLIFIC_PRODUCT_ID
					AND usbDevice.bcdDevice = TRANSPONDER_PROLIFIC_BCD
					THEN
					(* USB Prolific Transponder found *)
					strcpy(ADR(StringDevice), ADR(usbDevice.ifName));
					usbNodeId := usbNodeList[usbNodeIx];
					usbAction := USB_DEVICEOPEN;
				ELSE
					usbNodeIx := usbNodeIx + 1;
					IF usbNodeIx >= UsbNodeListGetFub.allNodes THEN
						(* USB Device NOT found *)
						usbAction := USB_GETNODELIST;
					END_IF
				END_IF
			ELSIF UsbNodeGetFub.status = asusbERR_USB_NOTFOUND THEN
				(* USB Device NOT found *)
				usbAction := USB_GETNODELIST;
			END_IF
	
		USB_DEVICEOPEN:
			(* initialize open structure *)
			
			FrameXOpenFub(enable := TRUE,
			device := ADR(StringDevice),
			mode := ADR('115200,8,N,1,/FCCTS=0'),
			config := 0);
		
			IF FrameXOpenFub.status = ERR_OK THEN
				usbAction := USB_READ_TOEND;
				Ident := FrameXOpenFub.ident; (* get ident *)
			ELSIF FrameXOpenFub.status = frmERR_DEVICEDESCRIPTION THEN
				usbAction := USB_GETNODELIST;
			END_IF
		
		USB_READTAG:
			(* initialize get buffer structure *)
			FrameGetBufferFub(enable := TRUE,ident := Ident);
			pSendBuffer := FrameGetBufferFub.buffer; (* get adress OF send buffer *)
			SendBufferLength := FrameGetBufferFub.buflng; (* get length OF send buffer *)
		
			IF FrameGetBufferFub.status = ERR_OK THEN	(* check status *)						
				memcpy(pSendBuffer, ADR(WriteData), strlen(ADR(WriteData))); (* copy write data into send buffer *)
				(* initialize write structure *)
				FrameWriteFub(enable := TRUE,ident := Ident,buffer := pSendBuffer,buflng := strlen(ADR(WriteData)));
			
				IF FrameWriteFub.status = ERR_OK THEN	(* check status *)
					usbAction := USB_READ_TOEND;
					(* initialize release output buffer structure *)
					FrameReleaseOutputBufferFub(enable := TRUE,ident := Ident,buffer := pSendBuffer,buflng := strlen(ADR(WriteData)));
				END_IF
			ELSIF FrameGetBufferFub.status = frmERR_NOTOPENED THEN
				usbAction := USB_GETNODELIST;
			END_IF
	
		USB_READ_TOEND:
			(* initialize read structure *)
			FrameReadFub(enable := TRUE,ident := Ident);
			pReadBuffer := FrameReadFub.buffer; (* get adress OF read buffer *)
			ReadBufferLength := FrameReadFub.buflng; (* get length OF read buffer *)
		
			IF (FrameReadFub.status = ERR_OK)  THEN (* check status *)
				memcpy(ADR(ReadData), pReadBuffer, ReadBufferLength); (* copy read data into ARRAY *)
				(* initialize release buffer structure *)
				FrameReleaseBufferFub(enable := TRUE,
					ident := Ident,
					buffer := pReadBuffer,
					buflng := ReadBufferLength);
			ELSIF FrameReadFub.status = frmERR_NOINPUT THEN 
				usbAction := USB_READ_TOEND;
			ELSE
				(* Read TO end *)
				usbAction := USB_GETNODELIST;
			END_IF
				
		USB_READ:
			strcpy(ADR(WriteData), ADR('read,0,0,#crc$R$L'));
			usbAction := USB_READTAG;
	END_CASE
			
	IF ((UsbNodeListGetFub.status = 32900) AND (usbAction = USB_GETNODELIST )) THEN
		//Module is disconnect
		gRFIDModule.ModuleDisconected := TRUE;
		//Hide the RFID layer
		gRFIDModule.RFIDlayer := 1;
		//If device is plugged then
	ELSIF (UsbNodeListGetFub.status = 0) THEN
		gRFIDModule.ModuleDisconected := FALSE;
		//Hide the RFID layer
		gRFIDModule.RFIDlayer := 0;
	END_IF;
		
	//Show RFID layer
	memcpy(ADR(data), ADR(ReadData), 11);
	//Compare if the message from RFID reader is about new attached chip
	IF (brsstrcmp(ADR(data),ADR('PiccSelect:'))=0) THEN	
		//Copy ID from PiccSelect message
		RFID_status_colour := 1;
		brsmemset(ADR(Tag_ID),0,SIZEOF(Tag_ID));
		brsmemcpy(ADR(Tag_ID),ADR(ReadData)+12, 8);
		Tag_ID_Delay(IN := TRUE, PT := 1000);
	ELSE
		IF NOT Tag_ID_Delay.Q AND Tag_ID_Delay.IN THEN
			Tag_ID_Delay(IN := TRUE, PT := 1000);
		ELSE 
			RFID_status_colour := 0;
			strcpy(ADR(Tag_ID), ADR('Disconnected'));
			Tag_ID_Delay(IN := FALSE, PT := 1000);
		END_IF;
	END_IF;
		
END_PROGRAM
