(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer 
 ********************************************************************
 * Program: 125kHzTransp
 * File: 125kHzTransp.typ
 * Author: Bernecker + Rainer
 * Created: September 15, 2009
 ********************************************************************
 * Local data types of program 125kHzTransp
 ********************************************************************)

TYPE
	usbAction_enum : 
		(
		USB_GETNODELIST,
		USB_SEARCHDEVICE,
		USB_DEVICEOPEN,
		USB_READTAG,
		USB_READ_TOEND,
		USB_READ
		);
END_TYPE
