(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * PROGRAM: SwInfo
 * File: SwInfo.st
 * Author: Bubenik
 * Created: August 14, 2018
 ********************************************************************
 * Implementation OF PROGRAM SwInfo
 ********************************************************************)

PROGRAM _INIT
	
	CfgGetIPAddr_0.enable := 1;
	CfgGetIPAddr_0.Len := 16;
	CfgGetIPAddr_0.pDevice := ADR('IF2');
	CfgGetIPAddr_0.pIPAddr := ADR(SwSysInfo.IPAdress);
	
	CfgGetInaNode_0.enable := 1;
	CfgGetInaNode_0.pDevice := ADR('IF2');
	SwSysInfo.InaNode := CfgGetInaNode_0.InaNode;
	
	ArProjectGetInfo_0.Execute := 1;
	ArProjectGetInfo_0();
	SwSysInfo.ASversion := ArProjectGetInfo_0.ConfigurationVersion;
	
END_PROGRAM

PROGRAM _CYCLIC
	
	CfgGetIPAddr_0();
	CfgGetInaNode_0();
	status := CfgGetInaNode_0.status;
		
END_PROGRAM


